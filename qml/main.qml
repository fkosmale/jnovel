import QtQuick 2.14
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.14
import QtQuick.Window 2.14
import QtQml 2.14
import club.jnovel.unofficial 1.0
import QtQuick.Controls.Material 2.14
import QtQuick.Controls.Universal 2.14

import "qrc:/qml/custom"

ApplicationWindow  {
    id: mainWindow

    property bool darkMode: false
    Material.theme: darkMode ? Material.Dark : Material.Light
    Universal.theme: darkMode ? Universal.Dark : Universal.Light

    visible: true
    color: palette.base
    visibility: Qt.platform.os === "android" && lNReadingStack && lNReadingStack.readerOpen ? Window.FullScreen : Window.Windowed

    title: (lNReadingStack.depth !== 3) ? "J-Novel Club Reader" : Connector.lastFetchedPart.title

    Component.onCompleted: {
        Connector.checkIfLoggedIn();
        if (Qt.platform.os === "android") {
            width = Screen.desktopAvailableWidth;
            height = Screen.desktopAvailableHeight;
        } else {
            width = 1200;
            height = 600;
        }
        setX(Screen.width / 2 - width / 2);
        setY(Screen.height / 2 - height / 2);
    }

    Component {
        id: partView
        PartReader {visible: lNReadingStack.visible}
    }

    Loader {
        id: latestPartsLoader
        active: false
        source: "qrc:/qml/custom/LatestReleasesDrawer.qml"
        asynchronous: true
    }

    ColumnLayout {
        anchors.fill: parent

        TabBar {
            id: bar
            Layout.fillWidth: true
            visible: !lNReadingStack.readerOpen
            TabButton {
                id: seriesOveriew
                text: "Series Overview"
            }

            TabButton {
                text: "Latest Releases"
            }

            TabButton {
                text: "Your Account"
            }
        }

        SwipeView {
            id: mainView
            interactive: false
            focus: true
            Layout.fillWidth: true
            Layout.fillHeight: true

            currentIndex: bar.currentIndex

            ReadingStack {
                id: lNReadingStack
                visible: SwipeView.isCurrentItem
            }

            LatestReleases {
                id: latestReleases
                readingStack: lNReadingStack
                visible: SwipeView.isCurrentItem
            }

            User {
                id: user
                visible: SwipeView.isCurrentItem
            }
        }
    }
}
