.pragma library
.import club.jnovel.unofficial 1.0 as JNC
.import QtQuick.Controls.Material 2.14 as Material
.import QtQuick.Controls.Universal 2.14 as Universal


const darkPalette = {
     window: "#323232",
     windowText: "white",
     base: "#323232",
     alternateBase: "#3C3C3C",
     text: "white",
     button: "#353535",
     buttonText: "white",
     brightText: "#0063A5",
     highlight: "#264F78",
     highlightedText: "white"
}

function scheme2id(scheme) {
    switch(scheme) {
    case "DARK":
        return 1;
    case "SEPIA":
        return 0;
    case "LIGHT":
        return 2;
    case "BLACK":
        return 3;
    }
    return 0;
}

function id2schemeName(scheme) {
    switch(scheme) {
    case 1:
        return "DARK"
    case 0:
        return "SEPIA"
    case 2:
        return "LIGHT"
    case 3:
        return "BLACK"
    }
    return ""
}

// TODO: avoid duplication
function rgba(r,g,b,a) {return Qt.rgba(r/255,g/255,b/255,a);}
function bgColor(currentIndex) {
    switch(currentIndex) {
    case 0:
    default:
        return rgba(244, 236, 216, 1);
    case 1:
        return rgba(51,51,51, 1);
    case 2:
        return rgba(255,255,255, 1);
    case 3:
        return rgba(0, 0, 0, 1)
    }
}

function fgColor(currentIndex) {
    switch(currentIndex) {
    case 0:
    default:
        return rgba(91, 70, 54, 1);
    case 1:
        return rgba(238,238,238, 1);
    case 2:
        return rgba(51,51,51, 1);
    case 3:
        return rgba(252, 252, 252, 1)
    }
}

function update(mainWindow, colorScheme) {
    switch(colorScheme) {
    case "BLACK":
        // fallthrough
    case JNC.ReadingNS.BLACK:
        mainWindow.Material.background = "black"
    case "DARK":
    case JNC.ReadingNS.DARK:
        mainWindow.darkMode = true
        useDarkPalette(true, mainWindow)
        break;
    default:
        mainWindow.darkMode = false
        useDarkPalette(false, mainWindow)
    }
}

function useDarkPalette(dark, mainWindow) {
    Object.keys(darkPalette).forEach( (key, index) =>
        mainWindow.palette[key] = dark ? darkPalette[key] : JNC.DefaultPalette.palette[key]
    )
}
