import QtQuick 2.14
import QtQuick.Layouts 1.14

Rectangle
{
    id: root
    signal goBack()
    signal goForward()

    property alias heading: headerText.text
    property alias hasForward: arrowRectForward.visible
    property alias hasBackward: arrowRectBackward.visible
    property alias hasSearch: searchRect.visible
    property alias searchPressed: searchRect.pressed

    height: headerText.contentHeight * 1.3
    color: "blue"
    Text {id: reference; visible: false}
    RowLayout {
        height: parent.height
        width: parent.width

        Rectangle {
            id: arrowRectBackward
            Layout.minimumWidth: height
            Layout.preferredHeight: parent.height * 0.8
            Layout.minimumHeight: 30
            Layout.leftMargin: 20
            Layout.alignment: Qt.AlignHCenter
            radius: 50
            color: Qt.lighter(root.color, 1.7)
            TapHandler {
                gesturePolicy: TapHandler.ReleaseWithinBounds
                onTapped: () => root.goBack()
                onDoubleTapped: () => root.goBack()
            }
            Image {
                source: "qrc:resources/arrow-back.png"
                anchors.centerIn: arrowRectBackward
                smooth: true
                mipmap: true
                width: 30
                height: 30
            }
        }

        Text {
            id: headerText
            font.pointSize: reference.font.pointSize*2
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "#FFE"
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        Rectangle {
            id: searchRect
            property bool pressed: false
            Layout.minimumWidth: height
            Layout.preferredHeight: parent.height * 0.8
            Layout.minimumHeight: 30
            Layout.rightMargin: 20
            Layout.alignment: Qt.AlignHCenter
            radius: 50
            border.color: mainWindow.palette.shadow
            border.width: pressed ? 1 : 0
            color: pressed ? mainWindow.palette.dark : Qt.lighter(root.color, 1.7)
            TapHandler {
                gesturePolicy: TapHandler.ReleaseWithinBounds
                onTapped: () => searchRect.pressed = !searchRect.pressed
                onDoubleTapped: () => searchRect.pressed = !searchRect.pressed
            }
            Image {
                source: "qrc:resources/search.png"
                anchors.centerIn: searchRect
                smooth: true
                mipmap: true
                width: 30
                height: 30
            }
        }

        Rectangle {
            id: arrowRectForward
            Layout.minimumWidth: height
            Layout.preferredHeight: parent.height * 0.8
            Layout.minimumHeight: 30
            Layout.rightMargin: 20
            Layout.alignment: Qt.AlignHCenter
            radius: 50
            color: Qt.lighter(root.color, 1.7)
            TapHandler {
                gesturePolicy: TapHandler.ReleaseWithinBounds
                onTapped: () => root.goForward()
                onDoubleTapped: () => root.goForward()
            }
            Image {
                source: "qrc:resources/arrow-forward.png"
                smooth: true
                mipmap: true
                anchors.centerIn: arrowRectForward
                width: 30
                height: 30
            }
        }

    }
}
