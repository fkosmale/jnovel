import QtQuick 2.13
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13
import club.jnovel.unofficial 1.0
import "qrc:/qml/palette.js" as ColorUtils


Item {

    property bool nextPartEnabled: true
    property bool prevPartEnabled: true

    property bool useLimitedView: (parent !== null) && parent.width < 940
    property bool useVeryLimitedView: (parent !== null) && parent.width < 600

    property alias currentPart: partPageView.currentPart
    property alias initialPercentage: partPageView.initialPercentage
    Component.onCompleted: partPageView.currentPart = currentPart

    function gotoPrevPart() {
        partPageView.prepareSequentialPartChange()
        var newPart = LNVolumeModel.sourceModel.getPrevPart(partPageView.currentPart)
        partPageView.currentPart = newPart
        partViewBusyIndicator.running = true
        if (LNVolumeModel.sourceModel.getPrevPart(newPart).partID === "")
            prevPartEnabled = false;
        nextPartEnabled = true
    }

    function gotoNextPart() {
        partPageView.prepareSequentialPartChange()
        var newPart = LNVolumeModel.sourceModel.getNextPart(partPageView.currentPart)
        partPageView.currentPart = newPart
        partViewBusyIndicator.running = true
        if (LNVolumeModel.sourceModel.getNextPart(newPart).partID === "")
            nextPartEnabled = false
        prevPartEnabled = true
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        spacing: 0
        PartPageView {
            id: partPageView
            Layout.fillWidth: true
            Layout.fillHeight: true
            useTwoPages: !useLimitedView
            connector: Connector
            currentlyDragged: dragHandler.active

            onParsingStarted: {
                partViewBusyIndicator.running = true;
            }

            onParsingEnded: {
                partViewBusyIndicator.running = false;
            }

            Component.onCompleted: {
                // "0" is the invalid ID
                if (LNVolumeModel.sourceModel.getPrevPart(Connector.lastFetchedPart).partID === "") {
                    prevPartEnabled = false;
                }
                if (LNVolumeModel.sourceModel.getNextPart(Connector.lastFetchedPart).partID === "")
                    nextPartEnabled = false;
                partPageView.keepScreenOn(true);
            }

            Component.onDestruction: {
                partPageView.keepScreenOn(false);
            }

            pageOffset: dragHandler.active ? dragHandler.translation.x : 0

            DragHandler {
                id: dragHandler
                target: null
                yAxis.enabled: false
                onActiveChanged: function() {
                    if (dragHandler.active) {
                        partPageView.pageMoving = true
                        return
                    }
                    const xVelocity = dragHandler.centroid.velocity.x
                    const xOffset = dragHandler.translation.x
                    if (partPageView.nextPageEnabled && (xVelocity < -1 || -xOffset > partPageView.width/9))
                        partPageView.gotoNextPage()
                    else if (partPageView.prevPageEnabled && (xVelocity > 1 || xOffset > partPageView.width/9))
                        partPageView.gotoPrevPage()
                    partPageView.pageMoving = false
                }
            }

            /*TapHandler {
                onSingleTapped: (eventPoint) => partPageView.handleClick(eventPoint.position.x, eventPoint.position.y)

            }*/

            BusyIndicator {
                id: partViewBusyIndicator
                anchors.centerIn: partPageView
                running: true
                width: parent.width / 2
                height: parent.height / 2
            }
        }


        RowLayout {
            Layout.fillWidth: true
            id: uiButtons
            Layout.alignment: Qt.AlignVCenter|Qt.AlignHCenter

            Button {
                text: "Back to Part Overview"
                visible: !useLimitedView
                onClicked: lNReadingStack.pop();
                Layout.alignment: Qt.AlignLeft
            }

            Button {
                id: prevPartButton
                font.family: "icomoon"
                text: "\uea18"
                enabled: prevPartEnabled
                onClicked: gotoPrevPart();
            }

            Button {
                id: prevButton
                visible: !useLimitedView
                font.family: "icomoon"
                text: "\uea44"
                enabled: partPageView.prevPageEnabled
                onClicked: partPageView.gotoPrevPage();
            }

            ComboBox {
                id: pageChooser
                function range(i) {
                    var arr = new Array(i);
                    for (var j = 0; j<i; j++)
                        arr[j] = j+1;
                    return arr;
                }
                model: range(partPageView.pageCount)
                currentIndex: partPageView.pageNumber - 1
                TextMetrics {id: reference; text: "999/999"}
                Layout.preferredWidth: reference.width +  2*leftPadding + 2*rightPadding
                displayText: "%1/%2".arg(partPageView.pageNumber).arg(partPageView.pageCount)
                onActivated: partPageView.pageNumber = index+1;
            }

            Button {
                visible: !useLimitedView
                text: partPageView.useTwoPages ? "Single Page" : "Book View \uD83D\uDCD6"
                onClicked: partPageView.useTwoPages = !partPageView.useTwoPages;
            }

            RowLayout {
                id: radioButtons
                visible: !useLimitedView
                ButtonGroup {
                    function scheme2button(scheme) {
                        switch(scheme) {
                        case ReadingNS.DARK:
                            return darkButton;
                        case ReadingNS.SEPIA:
                            return sepiaButton;
                        case ReadingNS.LIGHT:
                            return lightButton;
                        case ReadingNS.BLACK:
                            return blackButton;
                        }
                    }
                    id: readingMode
                    checkedButton: scheme2button(partPageView.colorScheme);
                }

                function switchColor(colorScheme) {
                    partPageView.setColorScheme(colorScheme)
                    ColorUtils.update(mainWindow, colorScheme)
                }

                RadioButton {
                    id: sepiaButton
                    text: "SEPIA"
                    ButtonGroup.group: readingMode
                    onClicked: radioButtons.switchColor(ReadingNS.SEPIA);
                }

                RadioButton {
                    id: darkButton
                    text: "DARK"
                    ButtonGroup.group: readingMode
                    onClicked: radioButtons.switchColor(ReadingNS.DARK);

                }
                RadioButton {
                    id: blackButton
                    text: "BLACK"
                    ButtonGroup.group: readingMode
                    onClicked: radioButtons.switchColor(ReadingNS.BLACK);

                }
                RadioButton {
                    id: lightButton
                    text: "LIGHT"
                    ButtonGroup.group: readingMode
                    onClicked: radioButtons.switchColor(ReadingNS.LIGHT);
                }
            }

            ComboBox {
                visible: !radioButtons.visible && !useVeryLimitedView
                id: readingModeComboBox

                function scheme2id(scheme) {
                    switch(scheme) {
                    case ReadingNS.DARK:
                        return 1;
                    default:
                    case ReadingNS.SEPIA:
                        return 0;
                    case ReadingNS.LIGHT:
                        return 2;
                    }
                }
                model: ListModel {
                    ListElement {key: "SEPIA"; value: 0}//0+ReadingNS.SEPIA}
                    ListElement {key: "DARK" ; value: 1}//0+ReadingNS.DARK}
                    ListElement {key: "LIGHT"; value: 2}//0+ReadingNS.Light}
                    }
                textRole: "key"
                onActivated: partPageView.setColorScheme(currentIndex);
                currentIndex: scheme2id(partPageView.colorScheme)
            }

            Button {
                id: nextButton
                visible: !useLimitedView
                font.family: "icomoon"
                text: "\uea42"
                enabled: partPageView.nextPageEnabled
                onClicked: partPageView.gotoNextPage()
            }

            Button {
                id: nextPartButton
                font.family: "icomoon"
                text: "\uea19"
                enabled: nextPartEnabled
                onClicked: gotoNextPart();
            }

        }
    }

    Keys.onPressed: {
        switch (event.key) {
            case Qt.Key_Right:
            case Qt.Key_Space:
                if (partPageView.nextPageEnabled)
                    Qt.callLater(partPageView.gotoNextPage);
                break;
            case Qt.Key_Left:
                if (partPageView.prevPageEnabled)
                    Qt.callLater(partPageView.gotoPrevPage);
                break;
            case Qt.Key_PageDown:
                if (prevPartEnabled)
                    Qt.callLater(gotoPrevPart);
                break;
            case Qt.Key_PageUp:
                if (nextPartEnabled)
                    Qt.callLater(gotoNextPart);
                break;
            default:
                event.accepted = false;
                return;
        }
        event.accepted = true;
    }

    Connections {
        target: partPageView
        onPercentReadChanged: {
            Connector.updatePartCompletionStatus(Connector.lastFetchedPart, percentage);
            Library.updatePercentRead(percentage, Connector.lastFetchedPart);
        }
    }
}
