import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12
import QtQml 2.12
import club.jnovel.unofficial 1.0

Item {
    id: mangaPagesView
    property int imageHeight: height
    anchors.fill: parent

    SwipeView {
        //id: inner
        id: inner
        LayoutMirroring.enabled: true
        LayoutMirroring.childrenInherit: true

        anchors.centerIn: parent
        width: mangaPagesView.width
        height: mangaPagesView.height

        Repeater {
            model: MangaImagesModel
            Loader {
                id: doublePageLoader
                property bool isCurrent: SwipeView.isCurrentItem
                active: SwipeView.isCurrentItem || SwipeView.isNextItem || SwipeView.isPreviousItem

                property double xScaleActive: 1
                property double yScaleActive: 1
                property double zoomX: 0
                property double zoomY: 0

                onIsCurrentChanged: function(){
                    doublePageLoader.xScaleActive = 1;
                    doublePageLoader.yScaleActive = 1;
                    doublePageLoader.zoomX = 0;
                    doublePageLoader.zoomY = 0;
                    console.log("reset scale");
                }

                sourceComponent: Flickable {
                    height: inner.height
                    width: inner.width
                    contentHeight: height*tform.yScale
                    contentWidth: width*tform.xScale
                    boundsBehavior: Flickable.StopAtBounds

                    MouseArea {
                        anchors.fill: parent
                        property double factor: 2.0
                        onWheel:
                        {
                            let zoomFactor = 1;
                            if(wheel.angleDelta.y > 0)  // zoom in
                                zoomFactor = factor;
                            else                        // zoom out
                                zoomFactor = 1/factor;

                            const realX = wheel.x * doublePageLoader.xScaleActive;
                            const realY = wheel.y * doublePageLoader.yScaleActive;
                            const oldXScale = doublePageLoader.xScaleActive;
                            const oldYScale = doublePageLoader.yScaleActive;
                            doublePageLoader.xScaleActive = Math.min(Math.max(doublePageLoader.xScaleActive * zoomFactor,1), 4)
                            doublePageLoader.yScaleActive = Math.min(Math.max(doublePageLoader.yScaleActive * zoomFactor,1), 4)
                            if (doublePageLoader.xScaleActive === oldXScale && doublePageLoader.yScaleActive === oldYScale) {
                                if (doublePageLoader.xScaleActive === doublePageLoader.yScaleActive && doublePageLoader.xScaleActive === 1) {
                                    doublePageLoader.zoomX = 1;
                                    doublePageLoader.zoomY = 1;
                                }
                            } else {
                                doublePageLoader.zoomX += (1-zoomFactor)*realX;
                                doublePageLoader.zoomY += (1-zoomFactor)*realY;
                            }

                        }
                    }

                    RowLayout {
                        id: pagesLayout
                        transform: Scale {
                            id: tform
                            xScale: doublePageLoader.isCurrent ? parent.xScaleActive : 1
                            yScale: doublePageLoader.isCurrent ? parent.yScaleActive : 1
                            //origin.x: (inner.x + inner.width)/2
                            //origin.y: (inner.y + inner.height)/2
                        }

                        height: parent.contentHeight
                        x: doublePageLoader.isCurrent ? doublePageLoader.zoomX : 0
                        y: doublePageLoader.isCurrent ? doublePageLoader.zoomY : 0
                        spacing: 0
                        Rectangle {
                            height: mangaPagesView.imageHeight
                            Layout.maximumWidth: inner.width / 2
                            Layout.preferredWidth: inner.width /2
                            Layout.minimumWidth: 100
                            color: "blue"
                            Image {
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: parent.right
                                height: parent.height
                                visible: hasPrevPage
                                source: hasPrevPage ? prevPage : ""
                                asynchronous: true
                                fillMode: Image.PreserveAspectFit
                                mipmap: true
                            }
                        }
                        Rectangle {
                            id: rec1
                            color: "red"
                            height: mangaPagesView.imageHeight
                            Layout.maximumWidth: inner.width / 2
                            Layout.preferredWidth: inner.width /2
                            Layout.minimumWidth: 100
                            Image {
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.left: parent.left
                                height: parent.height
                                source: display
                                fillMode: Image.PreserveAspectFit
                                mipmap: true
                            }
                        }
                    }
                }
            }
        }
    }
}
