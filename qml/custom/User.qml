import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import Qt.labs.settings 1.0

import club.jnovel.unofficial 1.0
import "qrc:/qml/palette.js" as ColorUtils

FocusScope {
    id: userPage
    Text {
        id: defaultFontProvider
        font.family: "Sans Serif"
        visible: false
        enabled: false
    }

    Column {
        width: parent.width
        height: parent.height

        Rectangle {
            id: loginStatusMessage
            height: message.height
            width: parent.width
            color: Connector.loginStatus === 3 ? "green" : (Connector.loginStatus === 1 ? "yellow" : "red")

            function status2message(status) {
                switch(status) {
                case 0:
                    return "You are not logged in. Only a limited amount of novels is accessible";
                case 2:readingModeComboBox
                    return "Login failed. Please check your username and password";
                case 3:
                    return "You are logged in";
                case 1:
                    return "Logging in…";
                }
            }

            Text {
                id: message
                width: parent.width
                height: implicitHeight*2
                text: loginStatusMessage.status2message(Connector.loginStatus)
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        ColumnLayout {
            id: loginFields
            visible: height > 0
            height: (Connector.loginStatus !== 3) ? implicitHeight : 0
            width: parent.width
            clip: true
            Behavior on height {
                NumberAnimation { duration: 500 }
            }

            TextField {
                id: emailField
                Layout.preferredWidth: userPage.width
                placeholderText: "Please enter your email address"
                text: (settings.emailAddress === undefined) ? "" : settings.emailAddress
                Keys.onReturnPressed: passField.forceActiveFocus()
            }

            function attemptLogin() {
                settings.emailAddress = emailField.text;

                Connector.login(emailField.text, passField.text);
            }

            TextField {
                id: passField
                Layout.preferredWidth: userPage.width
                placeholderText: "Please enter your password"
                text: (settings.password === undefined) ? "" : settings.password
                echoMode: TextInput.Password
                Keys.onReturnPressed: loginFields.attemptLogin();
            }

            Button {
                id: loginButton
                Layout.preferredWidth: userPage.width
                text: Connector.loggedIn ? "Logged in" : "Login"
                enabled: passField.text !== "" && emailField.text !== "" && !Connector.loggedIn && Connector.loginStatus !== 3
                onClicked: {
                    loginFields.attemptLogin();
                }
                BusyIndicator {
                    id: loggingInIndicator
                    anchors.fill: parent
                    width: parent.width
                    height: parent.height
                    running: Connector.loginStatus === 1;
                }
            }
        }

        Column {
            width: parent.width
            //height: previewText.contentHeight*1.1

            Settings {
                id: settings
                property alias font: defaultFontProvider.font
                property string colorScheme
                property string password
                property string emailAddress

                onFontChanged: {
                    fontComboBox.updateCurrentFont()
                    for (let i = 0; i<fontSizeField.model.length; ++i)
                        if (fontSizeField.model[i] === settings.font.pointSize) {
                            fontSizeField.currentIndex = i
                            break
                        }
                }

                Component.onCompleted: {
                    if (colorScheme === undefined || colorScheme === "")
                        colorScheme = "SEPIA";
                    ColorUtils.update(mainWindow, colorScheme)
                }
            }

            Column {
                width: parent.width
                spacing: 10
                Row {
                    height: fontComboBox.height
                    width: parent.width
                    spacing: 10
                    Label {
                        id: fontNameLabel
                        text: "Current font is:"
                        color: mainWindow.palette.text
                        verticalAlignment: Text.AlignVCenter
                        width: implicitWidth
                        height: fontComboBox.height
                    }
                    ComboBox {
                        id: fontComboBox
                        width: parent.width - fontNameLabel.width - 2*parent.spacing
                        model: Qt.fontFamilies()
                        function updateCurrentFont() {
                            for (var i=0; i<= model.length; ++i) {
                                if(model[i] === settings.font.family) {
                                    currentIndex = i;
                                    break;
                                }
                            }
                        }
                        onCurrentIndexChanged: {
                            if (model.length === 0)
                                return;
                            var oldFont = settings.font;
                            if (oldFont === undefined)
                                return;
                            oldFont.family = model[currentIndex];
                            settings.font = oldFont;
                        }
                    }
                }
                Row {
                    height: fontSizeField.height
                    width: parent.width
                    spacing: 10
                    Label {
                        id: fontSizeLabel
                        color: mainWindow.palette.text
                        text: "Current font size:"
                        width: implicitWidth
                        height: fontSizeField.height
                        verticalAlignment: Text.AlignVCenter
                    }
                    ComboBox {
                        id: fontSizeField
                        //height: implicitHeight*2
                        width: parent.width - fontSizeLabel.width - 2*parent.spacing
                        model: [] .concat(FontInfo.getFontSizes(settings.font)) // explicitly coerce QList into array to work around bug
                        onActivated: {
                            var oldFont = settings.font
                            if (oldFont === undefined)
                                oldFont = fontSizeLabel.font
                            console.log("Selected", model[currentIndex])
                            oldFont.pointSize = model[currentIndex]
                            settings.font = oldFont
                        }
                    }
                }
            }

            Row {
                height: readingModeComboBox.implicitHeight
                width: parent.width
                spacing: 10
                Label {
                    id: colorschemeLabel
                    color: mainWindow.palette.text
                    text: "Current color scheme:"
                    width: implicitWidth
                    height: readingModeComboBox.height
                    verticalAlignment: Text.AlignVCenter
                }

                ComboBox {
                    id: readingModeComboBox
                    height: implicitHeight
                    width: parent.width - colorschemeLabel.width - 2*parent.spacing


                    model: ["SEPIA", "DARK", "LIGHT", "BLACK"]
                    onActivated: () => {
                                     settings.colorScheme = model[currentIndex]
                                     ColorUtils.update(mainWindow, settings.colorScheme)
                                 }
                    currentIndex: ColorUtils.scheme2id(settings.colorScheme)
                }
            }


            Label {
                id: previewLabel
                color: mainWindow.palette.text
                text: "Preview of how the text is displayed:"
                verticalAlignment: Text.AlignVCenter
            }

            Rectangle {
                width: parent.width
                height: previewText.implicitHeight
                color: ColorUtils.bgColor(readingModeComboBox.currentIndex)
                clip: true
                Text {
                    anchors.centerIn: parent
                    id: previewText
                    text: "The quick brown fox jumps over the lazy dog\nPack my box with five dozen liquor jugs.\nSeveral fabulous dixieland jazz groups played with quick tempo."
                    color: ColorUtils.fgColor(readingModeComboBox.currentIndex)
                    font: settings.font !== undefined ? settings.font : previewLabel.font
                }
                Behavior on height {
                    NumberAnimation { duration: 500 }
                }
            }
        }

        Button {
            id: libraryButton
            property bool showLib: false
            width: parent.width
            enabled: Connector.loginStatus === 3
            text: showLib ? "Hide Library" : "Show Library"
            Layout.preferredWidth: userPage.width
            onClicked: showLib = !showLib;
        }

        GridView {
            id: libraryGrid
            width: parent.width
            height: parent.height
            visible: Connector.loginStatus === 3 && libraryButton.showLib
            clip: true
            model: Library
            cellWidth: 280
            cellHeight: 480
            delegate: Column {
                width: libraryGrid.cellWidth
                height: libraryGrid.cellHeight
                Image {
                    source: imageURL;
                    anchors.horizontalCenter: parent.horizontalCenter
                    sourceSize.width: 240
                    width: 240
                    height: 400
                    fillMode: Image.Pad
                }
                Text {
                    text: title
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: 240
                    wrapMode: Text.Wrap
                }

                Button {
                    text: "Download"
                    width: 240
                    enabled: false
                }
            }
        }
    }
}
