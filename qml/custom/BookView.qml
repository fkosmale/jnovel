import QtQuick 2.14
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.14
import club.jnovel.unofficial 1.0


Item {
    id: bookView

    property Item curIt
    property int atLeastThisMuch: 280 * 1.3
    property alias minimalRequiredHeight: seriesImageID.height
    property alias imageWidth: seriesImageID.width

    RowLayout {
        width: bookView.width
        height: bookView.height
        Image {
                id: seriesImageID
                Layout.alignment: Qt.AlignTop
                source: curIt ? curIt.imageURL  : ""
                mipmap: true
                smooth: true
                fillMode: Image.Pad

                BusyIndicator {
                    anchors.fill: seriesImageID
                    running:  seriesImageID.status !== Image.Ready && seriesImageID.status !== Image.Error
                }
        }

        Label {
            Layout.preferredWidth: bookView.width - seriesImageID.width*1.1
            Layout.fillWidth: true
            Layout.fillHeight: true
            elide: Text.ElideRight
            wrapMode: Text.Wrap
            font.pointSize: 14
            color: mainWindow.palette.text
            text: bookView.curIt ?
                `<b>Author:</b> ${bookView.curIt.author}<br />
                   <b>Illustrator:</b> ${bookView.curIt.illustratorOrArtist}<br />
                   <b>Translator:</b> ${bookView.curIt.translator}<br />
                   <b>Editor:</b> ${bookView.curIt.editor}<br />
                   <b>Description:</b><br />
                   ${bookView.curIt.description}
                  `
                  : ""
        }
    }
}
