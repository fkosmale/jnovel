import QtQuick 2.0
import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

import QtQuick.Controls.Material 2.12
import club.jnovel.unofficial 1.0

FocusScope {
    id: titleListView

    property CommonMetaDataSortFilterProxyModel titleModel
    property alias heading: listHeader.heading
    property bool smartphoneMode
    property ListView view: titleList
    property alias showForward: listHeader.hasForward
    property alias showBackward: listHeader.hasBackward

    property alias currentItem: titleList.currentItem
    property alias currentIndex: titleList.currentIndex
    property alias count: titleList.count

    signal myIndexChanged(int i)
    signal entrySelected(int index)
    signal refreshRequest()
    signal goBack()

    BusyIndicator {
        width: parent.width/3
        height: parent.height
        running: titleList.count === 0
        anchors.centerIn: parent
    }

    ColumnLayout {

        width: parent.width
        height: parent.height
        spacing: 0

        ListHeader {
            id: listHeader
            Layout.fillWidth: true
            onGoForward: () => titleListView.entrySelected(currentIndex)
            onGoBack: () => titleListView.goBack()
            onSearchPressedChanged: () => {
                                        if (searchPressed) {
                                            filterField.state = 'visible'
                                            filterField.forceActiveFocus()
                                        } else {
                                            filterField.clear()
                                            filterField.state = ''
                                        }
                                    }
        }

        TextField {
            id: filterField
            Layout.topMargin: 5
            Layout.bottomMargin: 5
            Layout.fillWidth: true
            height: 0
            visible: false
            Layout.preferredHeight: height
            placeholderText: "Filter series"
            color: mainWindow.palette.text
            onTextChanged: {
                titleModel.setFilterRegularExpression(filterField.text);
            }
            states: [
                State {
                    name: "visible"
                    PropertyChanges {
                        target: filterField
                        height: filterField.implicitHeight
                        visible: true
                    }
                }
            ]
            transitions: [
                Transition {
                    NumberAnimation {
                        property: "height"
                        duration: 200
                    }
                }
            ]
        }

        ListView {
            id: titleList
            focus: true

            Layout.fillWidth: true
            Layout.fillHeight: true

            ScrollBar.vertical: ScrollBar {
                policy: ScrollBar.AlwaysOn
            }
            clip:  true
            model: titleModel

            Keys.onUpPressed: if (currentIndex > 0) currentIndex -= 1
            Keys.onDownPressed: if (currentIndex < count - 1) currentIndex += 1
            Keys.onReturnPressed: titleListView.entrySelected(titleList.currentIndex)
            Keys.onEnterPressed: titleListView.entrySelected(titleList.currentIndex)

            Connections {
                target: titleList
                onCurrentItemChanged: {
                    myIndexChanged(currentIndex);
                }
            }

            onCountChanged: {
                // model has changed, make sure we select *something*
                currentIndex = 0;
            }

            boundsBehavior: Flickable.DragOverBounds
            boundsMovement: Flickable.StopAtBounds

            property int oldOvershoot: 0
            property int maxOvershoot: 100
            property real triggerFactor: 0.75

            onVerticalOvershootChanged: {
                if (verticalOvershoot === 0 && oldOvershoot > triggerFactor * maxOvershoot) {
                    oldOvershoot = 0;
                    refreshRequest();
                } else if (-verticalOvershoot > oldOvershoot) {
                    oldOvershoot = -verticalOvershoot;
                }
            }

            Item {
                id: refreshIndictor
                property int size: parent.height/7
                property int currentPos: Math.min(-titleList.verticalOvershoot, titleList.maxOvershoot)
                visible: titleList.verticalOvershoot < 0
                width: size
                height: size
                anchors.top: parent.top
                anchors.topMargin: currentPos
                anchors.horizontalCenter: parent.horizontalCenter

                ProgressCircle {
                    anchors.centerIn: parent
                    opacity: 0.5 + Math.min(1, (refreshIndictor.currentPos / (titleList.triggerFactor * titleList.maxOvershoot)))*0.5
                    size: parent.size
                    colorCircle: Material.accent
                    arcBegin: 0
                    arcEnd: Math.max(40, 360*(parent.anchors.topMargin / 100))
                    lineWidth: 10
                    animationDuration: 20
                    rotation: (refreshIndictor.currentPos / titleList.maxOvershoot)  * 360
                }
            }

            highlightMoveDuration: 0

            delegate: Rectangle {
                id: lvDelegate
                readonly property string imageURL : model.imageURL
                readonly property string author: model.author
                readonly property string illustratorOrArtist: model.illustratorOrArtist
                readonly property string translator: model.translator
                readonly property string editor: model.editor
                readonly property string description: model.description
                readonly property string seriesID: model.seriesID ? model.seriesID : ""
                height: rlayout.implicitHeight
                width: titleList.width
                color: index === currentIndex
                       ? mainWindow.palette.highlight
                       : (index % 2 == 0
                          ? mainWindow.palette.base
                          : mainWindow.palette.alternateBase)

                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        if (smartphoneMode) {
                            titleList.currentIndex = index;
                            titleListView.entrySelected(index);
                        } else {
                            titleList.focus = true
                            titleList.currentIndex = index;
                        }
                    }

                    onDoubleClicked: {
                        titleList.currentIndex = index;
                        titleListView.entrySelected(index);
                    }
                }

                RowLayout {
                    id: rlayout
                    spacing: 0
                    width: titleList.width

                    Rectangle {
                        Layout.minimumHeight: 120
                        Layout.minimumWidth: 80
                        Layout.preferredWidth: 80
                        color: lvDelegate.color

                        Image {
                            source: previewImageURL
                            fillMode: Image.PreserveAspectFit
                            sourceSize.height: 120
                            smooth: true
                        }

                    }

                    Rectangle {
                        id: textRect
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        focus: true
                        color: lvDelegate.color

                        Label {
                            id: titleLabel
                            text: title
                            font.pointSize: 12
                            width: parent.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.Wrap
                            color: index === currentIndex ? mainWindow.palette.highlightedText : mainWindow.palette.text
                        }
                    }

                    Rectangle {
                        id: arrowRect
                        visible: !titleListView.smartphoneMode
                        Layout.preferredHeight: 50
                        Layout.preferredWidth: 50
                        Layout.minimumWidth: 50
                        Layout.rightMargin: 20
                        Layout.alignment: Qt.AlignHCenter
                        radius: 160
                        color: Qt.darker(textRect.color, 2.0)
                        TapHandler {
                            gesturePolicy: TapHandler.ReleaseWithinBounds

                            onTapped: {
                                titleList.currentIndex = index;
                                titleListView.entrySelected(index);
                            }

                            onDoubleTapped: {
                                titleList.currentIndex = index;
                                titleListView.entrySelected(index);
                            }
                        }
                        Image {
                            source: "qrc:resources/arrow-forward.png"
                            anchors.centerIn: arrowRect
                            width: 40
                            height: 40
                        }
                    }
                }
            }
        }

    }
}
