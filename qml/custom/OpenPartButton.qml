import QtQuick 2.13
import QtQuick.Controls 2.13
import club.jnovel.unofficial 1.0

Button {
    property string partTitle: title // make attached model property accessible
    property string partSlug: titleSlug
    property string partId: partID
    text: `Part ${index+1} (${(readingStack.readerOpen, Library.getPercentRead(partID)*100).toFixed(2)}%)${enabled ? "" : "\ue98f"}`
    enabled: adapter.isAccessible(Connector.loggedIn)
    font.family: "icomoon"
    width: textQuery.width
    ToolTip.visible: hovered
    ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
    ToolTip.text: enabled ? ("Open " + text) : ("Expired")
    onClicked: () => {
        let initialState = {
            currentPart: adapter,
            initialPercentage: Library.getPercentRead(partID)
        }
        readingStack.push(partView, initialState)
    }
}
