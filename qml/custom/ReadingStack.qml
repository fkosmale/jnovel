import QtQuick 2.13
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13
import club.jnovel.unofficial 1.0

FocusScope {
    id: readingStack
    function push(x, y) {if (x === partView) {stack.push(x, y, StackView.Immediate)} else  stack.push(x, y)}
    function pop(x) {if (stack.currentItem instanceof PartReader) {stack.pop(x !== undefined ? x : null, StackView.Immediate)} else {stack.pop(x)}}
    property bool readerOpen: stack.currentItem instanceof PartReader

    StackView {
        id: stack
        anchors.fill: parent

        Keys.onPressed: (event) => {
            if ((event.key === Qt.Key_Escape) || (event.key === Qt.Key_Back)) {
                if (stack.currentItem.pop && stack.currentItem.pop())
                    event.accepted = true;
                else if (stack.depth > 1) {
                    readingStack.pop();
                    event.accepted = true;
                }
            }
        }

        initialItem: SeriesView {
            id: aSeriesView
        }

        pushEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0.1
                duration: 200
            }
        }
        popEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        popExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0.1
                duration: 200
            }
        }

        onCurrentItemChanged: {
            if (currentItem) {
                currentItem.focus = true;
                currentItem.forceActiveFocus();
            }
        }

    }
}
