import QtQuick 2

SequentialAnimation {
    id: refreshAnimation
    property alias target: forwardAnimation.target

    NumberAnimation {
        id: forwardAnimation
        target: seriesListView
        property: "opacity"
        from: 1
        to: 0
        duration: 500
        easing.type: Easing.InOutQuad
    }


    NumberAnimation {
        target: refreshAnimation.target
        property: "opacity"
        from: 0
        to: 1
        duration: 500
        easing.type: Easing.OutInQuad
    }
}
