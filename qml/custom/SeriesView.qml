import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import club.jnovel.unofficial 1.0

FocusScope {
    id: theSeriesView

    function openVolume() {
        if (readingStack.depth > 1) {return;}
        Connector.fetchLNVolumesForID(seriesListView.currentItem.currentItem.seriesID);
        seriesListView.push(singleSeriesTLV);
    }

    function pop() {
        if (seriesListView.depth <= 1)
            return false
        seriesListView.pop()
        return true
    }

    RefreshAnimation {
        id: refreshAnimation
        target: seriesListView.currentItem.view
    }

    Connections {
        target: seriesListView.currentItem
        onRefreshRequest: {
            Connector.refreshVolumes();
            refreshAnimation.restart();
        }
    }

    Component {
        id: partList
        FocusScope {
            property var currentItem: seriesListView.get(1).currentItem
            property alias view: actualPartList
            signal refreshRequest()
            ColumnLayout {

                width: parent.width
                height: parent.height
                spacing: 0
                ListHeader {
                    id: listHeader
                    heading: "Select Part"
                    Layout.fillWidth: true
                    hasForward: false
                    onGoBack: () => seriesListView.pop()
                }
                ListView {
                    id: actualPartList
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ScrollBar.vertical: ScrollBar {
                        policy: ScrollBar.AlwaysOn
                    }
                    clip:  true
                    model: LNPartsModel
                    delegate: OpenPartButton {
                        width: parent.width
                    }
                }
            }
        }
    }

    Component {
        id: singleSeriesTLV
        TitleListView {
            smartphoneMode: !currentVolume.visible
            heading: "Select Volume"
            titleModel: LNVolumeModel
            showBackward: !smartphoneMode
            showForward: !smartphoneMode

            onMyIndexChanged: {
                titleModel.sourceModel.selectPartList(i);
            }

            onEntrySelected: () => seriesListView.push(partList)
            onGoBack: () => seriesListView.pop()
        }
    }

    Component {
        id: allSeriesTLV
        TitleListView {
            showForward: !smartphoneMode
            showBackward: false
            smartphoneMode: !currentVolume.visible
            Layout.preferredWidth: bookView.visible ? parent.width / 3 : parent.width
            Layout.minimumWidth: parent.width /10
            Layout.fillHeight: true
            heading: "Select Series"
            onEntrySelected: () => theSeriesView.openVolume();
            titleModel: FilterableLNSeriesModel
        }
    }

    RowLayout {

        anchors.fill: parent
        Layout.topMargin: 0

        StackView {
            id: seriesListView
            clip: true
            Layout.alignment: Qt.AlignTop
            Layout.preferredWidth: currentVolume.visible ? parent.width / 3 : parent.width
            Layout.minimumWidth: parent.width /10
            Layout.fillHeight: true
            property var curIt: currentItem.currentItem
            initialItem: allSeriesTLV
        }


        ColumnLayout {
            id: currentVolume
            Layout.fillHeight: true

            Layout.preferredHeight: parent.height
            Layout.preferredWidth: parent.width*2/3
            Layout.fillWidth: true
            visible: parent.width*2/3 >= bookView.atLeastThisMuch  && parent.height >= bookView.minimalRequiredHeight

            BookView {
                id: bookView
                Layout.alignment: Qt.AlignTop
                Layout.preferredWidth: currentVolume.width
                Layout.minimumWidth: bookView.atLeastThisMuch
                Layout.minimumHeight: bookView.minimalRequiredHeight
                Layout.preferredHeight: bookView.minimalRequiredHeight
                curIt: seriesListView.currentItem.currentItem ? seriesListView.currentItem.currentItem : seriesListView.get(0).currentItem
            }


            GridView {
                id: partGridView
                clip: true
                ScrollBar.vertical: ScrollBar {
                    policy: ScrollBar.AsNeeded
                }
                Layout.preferredWidth: parent.width
                Layout.minimumHeight: cellHeight*2
                Layout.preferredHeight: cellHeight*3
                model: LNPartsModel
                cellHeight: Math.min(textQuery.height * 3, 50);
                cellWidth: textQuery.width * 1.1
                visible: seriesListView.depth > 1

                TextMetrics  {
                    id: textQuery
                    font.family: "icomoon"
                    text: ">>Part 10 (100.00%) \ue98f<<"
                }

                delegate: OpenPartButton {}
            }
        }
    }
}
