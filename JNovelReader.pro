# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

TARGET = JNovelReader

QT += core gui network qml quick quickcontrols2
android {
    QT += androidextras
}

include(./android_openssl/openssl.pri)

TEMPLATE = app

CONFIG+=c++17
# CONFIG+=exceptions_off
CONFIG+=rtti_off
# CONFIG+=sanitizer sanitize_undefined

HEADERS = \
   src/commonmetadata.h \
   src/commonmetadatasortfilterproxymodel.h \
   src/jnovelclubconnector.h \
   src/json_helper.h \
   src/mangaimageprovider.h \
   src/mangaimagesmodel.h \
   src/pageview.h \
   src/partlistmodel.h \
   src/parts.h \
   src/readingmode.h \
   src/series.h \
   src/serieslistmodel.h \
   src/user.h \
   src/volume.h \
   src/volumelistmodel.h \
   src/booktype.h \
   src/partlist.h \
   src/serieslist.h \
   src/volumelist.h \
   src/flyweighthelper.h \
   src/fontinfo.h \


VERSION = 0.5.0

android {
    defineReplace(droidVersionCode) {
            segments = $$split(1, ".")
            for (segment, segments): vCode = "$$first(vCode)$$format_number($$segment, width=3 zeropad)"
            contains(ANDROID_TARGET_ARCH, arm64-v8a): \
                suffix = 1
            else:contains(ANDROID_TARGET_ARCH, armeabi-v7a): \
                suffix = 0
            # add more cases as needed
            return($$first(vCode)$$first(suffix))
    }
    ANDROID_VERSION_NAME = $$VERSION
    ANDROID_VERSION_CODE = $$droidVersionCode($$ANDROID_VERSION_NAME)
    HEADERS += src/android_keystore.h
}

SOURCES = \
   src/commonmetadata.cpp \
   src/commonmetadatasortfilterproxymodel.cpp \
   src/jnovelclubconnector.cpp \
   src/json_helper.cpp \
   src/main.cpp \
    src/mangaimageprovider.cpp \
    src/mangaimagesmodel.cpp \
   src/pageview.cpp \
   src/partlistmodel.cpp \
   src/parts.cpp \
   src/series.cpp \
   src/serieslistmodel.cpp \
   src/user.cpp \
   src/volume.cpp \
   src/volumelistmodel.cpp \
   src/fontinfo.cpp \

android {
    SOURCES += src/passencryption.cpp
}

INCLUDEPATH = \
    src
INCLUDEPATH += src/3rdparty

RESOURCES = jnovelreader.qrc
# CONFIG += qtquickcompiler
CONFIG += rtti_off

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x50110

DISTFILES += \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
