#ifndef MANGAIMAGESMODEL_H
#define MANGAIMAGESMODEL_H

#include <QAbstractListModel>

class MangaImagesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit MangaImagesModel(QObject *parent = nullptr);

    enum MangaImageRoles {
        PrevPageRole = Qt::UserRole + 1,
        HasPrevPageRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setImagePath(std::vector<QString> imagePaths);

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    std::vector<QString> m_imagePaths;
};

#endif // MANGAIMAGESMODEL_H
