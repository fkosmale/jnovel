#include <QPainter>
#include <QTextDocument>
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>
#include <QQmlEngine>
#include <QQmlContext>
#include <QAbstractTextDocumentLayout>
#include <QPropertyAnimation>
#include <QString>
#include <QSettings>
#include <QTimer>
#include <QMetaEnum>
#include <QFuture>
#include <QMutexLocker>
#include <QtGlobal>
#include <QGuiApplication>
#include <QTextBlock>
#include <QTextCursor>
#include <QMouseEvent>
#include <QLoggingCategory>
#include <QSGImageNode>
#include <QQuickWindow>

Q_LOGGING_CATEGORY(partreader, "jnovel.partreader")

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras>
#endif

#include <boost/container/small_vector.hpp>

#include <cmath>
#include <algorithm>
#include <memory>

#include "pageview.h"
#include "jnovelclubconnector.h"

constexpr CacheEntry prev_prev_page = 0;
constexpr CacheEntry prev_page = 1;
constexpr CacheEntry current_page = 2;
constexpr CacheEntry next_page = 3;
constexpr CacheEntry next_next_page = 4;
constexpr CacheEntry next_next_next_page = 5;


namespace  {
const constexpr auto betweenPagesOffset = 30;

[[nodiscard]] inline QImage resizeIfNecessary(const QImage& image, qreal width, qreal height, qreal margin) {
    auto pxr = qGuiApp->devicePixelRatio();
    margin *= 2; // top + bottom
    width  = (width - margin) * pxr;
    height = (height - margin) * pxr;
    if (image.height() >= height || image.width() >= width) {
        auto scaled = image.scaled( QSize { static_cast<int>(std::floor(width)), static_cast<int>(std::floor(height))}, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        return scaled;
    } else {
        return image;
    }
}
}

struct ImageResizer : QRunnable
{
    ImageResizer(qreal margin, QImage const& origImage, QString imageUrl, PageView* pageView, bool isResize=false)
        : m_isResize(isResize), m_margin(margin), m_origImage(origImage), m_imageUrl(imageUrl), m_pageView(pageView) {
        setAutoDelete(true);
    }
    bool m_isResize;
    qreal m_margin;
    QImage m_origImage;
    QString m_imageUrl;
    PageView* m_pageView;

    void run() override {
        auto imageVariant = QVariant{resizeIfNecessary(m_origImage, m_pageView->pageWidth(), m_pageView->height(), m_margin)};
        QMutexLocker lock(&m_pageView->m_documentMutex);
        m_pageView->m_textDocument.addResource(QTextDocument::ImageResource, QUrl(m_imageUrl), imageVariant);
        --m_pageView->m_resizeTasks;
        if (m_pageView->m_resizeTasks == 0) {
            if (!m_isResize) {
                QMetaObject::invokeMethod(m_pageView, &PageView::setTextDone, Qt::QueuedConnection);
            } else {
                QMetaObject::invokeMethod(m_pageView, &PageView::handleResizeDone, Qt::QueuedConnection);
            }
        }
    }
};

PageView::PageView(QQuickItem *parent) : QQuickItem(parent), m_pageOffset(0), m_pageNumber(1), m_pageCount(1)
{
    /*this->setOpaquePainting(true); // we fill everything, hence use opaque painting for improved speed
    this->setMipmap(true);
    this->setRenderTarget(QQuickPaintedItem::FramebufferObject);*/
    this->setFlag(QQuickItem::ItemHasContents);

    QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
    auto metaEnum = QMetaEnum::fromType<Reading::ColorScheme>();
    auto defaultVal = metaEnum.valueToKey(static_cast<int>(Reading::ColorScheme::DARK));
    const auto loaded  = settings.value(QStringLiteral("colorScheme"), defaultVal).toString().toStdString();
    setColorScheme(static_cast<Reading::ColorScheme>(metaEnum.keyToValue(loaded.c_str())));

    m_font = settings.value(QStringLiteral("font"), QFont(QStringLiteral("Literata"), 14)).value<QFont>();
    QMutexLocker lock {&m_documentMutex};
    m_textDocument.setDefaultFont(m_font);

    m_textDocument.setDefaultStyleSheet(QStringLiteral("p {text-indent: 10px; margin-top: 0; margin-bottom: 0;} .image {text-indent: 0px; margin-bottom: 8px}"));
    m_offsetAnimation = new QPropertyAnimation(this, "pageOffset", this);
    m_offsetAnimation->setDuration(200);
    m_offsetAnimation->setEasingCurve(QEasingCurve::InOutSine);

    connect(this, &PageView::widthChanged, this, &PageView::handleResize, Qt::QueuedConnection);
    connect(this, &PageView::heightChanged, this, &PageView::handleResize, Qt::QueuedConnection);
    connect(qGuiApp, &QGuiApplication::applicationStateChanged, this, &PageView::handleApplicationStateChange);
}

void PageView::handleResize() {
    const auto pageWidth =  this->pageWidth();
    const auto pageHeight = this->height();

    if (pageWidth <= 0 || pageHeight <= 0) {
        // happens on Android, causes crashes if we do not exit here
        return;
    }

    m_documentMutex.lock();
    auto previousSize = m_textDocument.pageSize();
    m_documentMutex.unlock();
    if (qFuzzyCompare(pageWidth, previousSize.width()) && qFuzzyCompare(pageHeight, previousSize.height())) {
        return;
    }
    m_documentMutex.lock();
    m_textDocument.setPageSize(QSizeF {pageWidth, pageHeight});
    m_documentMutex.unlock();
    emit parsingStarted();
    if (!m_URL2originalImage.empty()) {
        m_documentMutex.lock();
        const auto html = m_textDocument.toHtml();
        const auto margin = m_textDocument.documentMargin();

        // find current text position
        auto layout = m_textDocument.documentLayout();
        // position cursor in center of page
        // TODO: there is probably a better point than the center (and is one single point enough?)
        auto x = this->pageWidth() + 1;
        auto y = (m_pageNumber) * this->height() + 1;
        m_textPositionAtTopOfPage = layout->hitTest({x, y}, Qt::FuzzyHit);

        m_textDocument.clear();
        m_textDocument.setHtml(html); // set clear dance to clear ressources while still keeping the text
        m_resizeTasks = m_URL2originalImage.size();
        m_documentMutex.unlock();

        for (auto it = m_URL2originalImage.constBegin(); it != m_URL2originalImage.constEnd(); ++it) {
            const QImage image = it.value();
            QString imageURL = it.key();
            auto runnable = new ImageResizer(margin, image, imageURL, this, true);
            pool.start(runnable);
        }

    } else {
        handleResizeDone();
    }
}

void PageView::handleResizeDone() {
    emit parsingEnded();

    m_documentMutex.lock();
    m_textDocument.setHtml(m_textDocument.toHtml());
    const auto newPageCount = m_textDocument.pageCount();
    if (m_pageCount != newPageCount) {
        // try to find a close position on page
        int newPageNumber = 0;
        newPageNumber = std::max(static_cast<int>(std::floor(m_pageNumber * m_pageCount / newPageCount)), 1);
        // does not actually work
        /*if (m_textPositionAtTopOfPage == -1) {
        } else {
            auto layout = m_textDocument.documentLayout();
            auto const x = this->pageWidth() / 2;
            auto posAtPage = layout->hitTest({x, newPageNumber * this->height()}, Qt::FuzzyHit) ;
            while(posAtPage < m_textPositionAtTopOfPage && newPageNumber <= pageCount() && posAtPage != -1) {
                ++newPageNumber;
                posAtPage = layout->hitTest({x, newPageNumber * this->height()}, Qt::FuzzyHit);
            }
            newPageNumber = std::min(1, newPageNumber-1);
        }*/
        m_pageCount = newPageCount;
        emit pageCountChanged();
        setPageNumber(newPageNumber);
    }
    m_documentMutex.unlock();
    asyncPopulateCache();
}

void PageView::setupPainter(QPainter* painter) {
    painter->setRenderHint(QPainter::TextAntialiasing);
    auto bgBrush = QBrush{painter->background()};
    using namespace Reading;
    bgBrush.setColor(mode2bgColor(m_currentColorScheme));
    painter->setBackground(bgBrush);
    auto pen = QPen{painter->pen()};
    pen.setColor(mode2fgColor(m_currentColorScheme));
    painter->setPen(pen);
    painter->setFont(m_font);
}

void PageView::renderPage(QPainter* painter, int pageNumber) {
    const auto pageWidth = this->pageWidth();
    const auto pageHeight = this->height();
    const QRectF textRect = QRectF {0, 0, pageWidth, pageHeight};
    qreal documentHeight, documentWidth;
    {
        QMutexLocker lock(&m_documentMutex);
        documentHeight = m_textDocument.pageSize().height();
        documentWidth = m_textDocument.pageSize().width();
    }
    const QRectF textPageRect(0, pageNumber * documentHeight, documentWidth, documentHeight);
    setupPainter(painter);
    painter->save();
    // Clip the drawing so that the text of the other pages doesn't appear in the margins
    painter->setClipRect(textRect);
    // Translate so that 0,0 is now the page corner
    painter->translate(0, -textPageRect.top());
    // Translate so that 0,0 is the text rect corner
    painter->translate(textRect.left(), textRect.top());

    // using the context is necessary, else the font color is wrong
    QAbstractTextDocumentLayout::PaintContext ctx;
    ctx.palette.setColor(QPalette::Text, painter->pen().color());
    ctx.clip = textPageRect;

    QMutexLocker lock(&m_documentMutex);
    m_textDocument.documentLayout()->draw(painter, ctx);
    painter->restore();
}

void PageView::addCacheEntry(const CacheEntry entry, const int page) {
    const auto idx = static_cast<PageCache::size_type>(entry);
    const auto pixelRatio = qGuiApp->devicePixelRatio();
    const auto adjustedWidth = static_cast<int>(this->pageWidth()*pixelRatio);
    const auto adjustedHeight = static_cast<int>(this->height()*pixelRatio);
    m_pageCache[idx] = {}; // clear memory of current image before creating a new one
    if (doesPageContainImportantPicture(page)) {
        m_pageCache[idx] = QImage {adjustedWidth, adjustedHeight, QImage::Format_ARGB32_Premultiplied};
    } else {
        m_pageCache[idx] = QImage {adjustedWidth, adjustedHeight, QImage::Format_RGB16};
    }
    m_pageCache[idx].setDevicePixelRatio(pixelRatio);
    m_pageCache[idx].fill(Reading::mode2bgColor(m_currentColorScheme));
    QPainter painter(&m_pageCache[idx]);
    renderPage(&painter, page);
}

void PageView::populateCache() {
    const auto pageWidth = static_cast<int>(this->pageWidth());
    const auto pageHeight = static_cast<int>(this->height());
    if (pageWidth <= 0 || pageHeight <= 0) {
        return;
    }

    int currentPage = m_pageNumber - 1;
    if (m_useTwoPages) {
        addCacheEntry(prev_prev_page, currentPage-2);
    }
    addCacheEntry(prev_page, currentPage-1);
    addCacheEntry(current_page, currentPage);
    addCacheEntry(next_page, currentPage+1);
    if (m_useTwoPages) {
        addCacheEntry(next_next_page, currentPage+2);
        addCacheEntry(next_next_next_page, currentPage+3);
    }
    auto const pxr = qGuiApp->devicePixelRatio();
    m_pageSeparator = {};
    m_pageSeparator = {int(betweenPagesOffset*pxr), int(height()*pxr), QImage::Format::Format_ARGB32_Premultiplied};
    m_pageSeparator.setDevicePixelRatio(pxr);
    {
        QPainter painter(&m_pageSeparator);
        drawPageSeparator(&painter);
    }
}

void PageView::asyncPopuateHelper() {
    {
        QMutexLocker lock(&m_documentMutex);
        if (m_textDocument.toRawText().isEmpty())
                return;
    }
    populateCache();
    update();
}

void PageView::asyncPopulateCache() {
    QTimer::singleShot(0, this, &PageView::asyncPopuateHelper);
}

void PageView::cacheBackward(int dist) {
    int currentPage = m_pageNumber - 1;
    if (m_useTwoPages) {
        std::rotate(m_pageCache.rbegin(), m_pageCache.rbegin()+dist, m_pageCache.rend());
        addCacheEntry(prev_prev_page, currentPage-2);
        if (dist == 2)
            addCacheEntry(prev_page, currentPage-1);
    } else {
        m_pageCache[next_page] = std::move(m_pageCache[current_page]);
        m_pageCache[current_page] = std::move(m_pageCache[prev_page]);
        addCacheEntry(prev_page, currentPage-1);
    }
    auto const pxr = qGuiApp->devicePixelRatio();
    m_pageSeparator = {};
    m_pageSeparator = {int(betweenPagesOffset*pxr), int(height()*pxr), QImage::Format::Format_ARGB32_Premultiplied};
    m_pageSeparator.setDevicePixelRatio(pxr);
    {
        QPainter painter(&m_pageSeparator);
        drawPageSeparator(&painter);
    }
    this->update();
}

void PageView::cacheForward(int dist) {
    int currentPage = m_pageNumber - 1;
    if (m_useTwoPages) {
        std::rotate(m_pageCache.begin(), m_pageCache.begin()+dist, m_pageCache.end());
        addCacheEntry(next_next_next_page, currentPage+3);
        if (dist == 2)
            addCacheEntry(next_next_page, currentPage+2);
    } else {
        // rotate deemed too inpractical for the small range
        // it is VITAL to use std::copy, as the copy constructor seems to be still broken @re:https://bugreports.qt.io/browse/QTBUG-58653
        m_pageCache[prev_page] = std::move(m_pageCache[current_page]);
        m_pageCache[current_page] = std::move(m_pageCache[next_page]);
        addCacheEntry(next_page, currentPage+1);
    }
    auto const pxr = qGuiApp->devicePixelRatio();
    m_pageSeparator = {};
    m_pageSeparator = {int(betweenPagesOffset*pxr), int(height()*pxr), QImage::Format::Format_ARGB32_Premultiplied};
    m_pageSeparator.setDevicePixelRatio(pxr);
    {
        QPainter painter(&m_pageSeparator);
        drawPageSeparator(&painter);
    }
    this->update();
}


void PageView::drawPageSeparator(QPainter* painter) {
    using namespace Reading;
    auto pageHeight = this->height();
    painter->save();{
        {
            QRectF rectangle1 {0, 0, betweenPagesOffset/2, pageHeight};
            QLinearGradient gradient {0, 0, betweenPagesOffset/2, 0};
            gradient.setColorAt(0, mode2bgColor(m_currentColorScheme));
            gradient.setColorAt(1, mode2bgColor(m_currentColorScheme).darker());
            QBrush brush{gradient};
            painter->fillRect(rectangle1, brush);
        }
        {
            QRectF rectangle2 {0 + betweenPagesOffset/2, 0, betweenPagesOffset/2, pageHeight};
            QLinearGradient gradient {0 + betweenPagesOffset/2, 0, betweenPagesOffset, 0};
            gradient.setColorAt(0, mode2bgColor(m_currentColorScheme).darker());
            gradient.setColorAt(1, mode2bgColor(m_currentColorScheme));
            QBrush brush {gradient};
            painter->fillRect(rectangle2, brush);
        }
    }painter->restore();
}

QSGNode *PageView::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData)
{
    Q_UNUSED(updatePaintNodeData)

    constexpr int current = 1;
    constexpr int prev = 2;
    constexpr int next = 3;

    constexpr int next_next = 4;
    constexpr int prev_prev = 5;
    constexpr int next_next_next = 6;

    auto createImageNode = [this]() {
        auto ret = window()->createImageNode();
        ret->setFlag(QSGNode::OwnedByParent);
        return ret;
    };
    bool initialized = oldNode != nullptr;
    const auto pixelRatio = qGuiApp->devicePixelRatio();
    if (!initialized) {
        oldNode = new QSGNode;
        oldNode->setFlag(QSGNode::OwnedByParent);

        auto pageSeparatorHolderNode = new QSGNode;
        pageSeparatorHolderNode->setFlag(QSGNode::OwnedByParent);
        oldNode->appendChildNode(pageSeparatorHolderNode);
        pageSeparatorHolderNode->appendChildNode(createImageNode());
        pageSeparatorHolderNode->appendChildNode(createImageNode());

        oldNode->appendChildNode(createImageNode());  // current
        oldNode->appendChildNode(createImageNode());  // prev
        oldNode->appendChildNode(createImageNode());  // next
    }
    if (m_useTwoPages && oldNode->childCount() == 4) {
        oldNode->appendChildNode(createImageNode());  // next_next
        oldNode->appendChildNode(createImageNode());  // prev_prev
        oldNode->appendChildNode(createImageNode());  // next_next_next

        auto pageSeparatorHolderNode = oldNode->firstChild();
        pageSeparatorHolderNode->appendChildNode(createImageNode());
        pageSeparatorHolderNode->appendChildNode(createImageNode());
        pageSeparatorHolderNode->appendChildNode(createImageNode());
        initialized = false;
    } else if (!m_useTwoPages && oldNode->childCount() > 4) {
        QScopedPointer nextNextPageNode { oldNode->childAtIndex(next_next) };
        QScopedPointer prevPrevNode { oldNode->childAtIndex(prev_prev) };
        QScopedPointer nextNextNextPageNode { oldNode->childAtIndex(next_next_next) };
        oldNode->removeChildNode(nextNextPageNode.get());
        oldNode->removeChildNode(prevPrevNode.get());
        oldNode->removeChildNode(nextNextNextPageNode.get());

        auto separatorHolder { oldNode->firstChild() };
        QScopedPointer separatorNode2 { separatorHolder->childAtIndex(2) };
        QScopedPointer separatorNode3 { separatorHolder->childAtIndex(3) };
        QScopedPointer separatorNode4 { separatorHolder->childAtIndex(4) };
        separatorHolder->removeChildNode(separatorNode2.get());
        separatorHolder->removeChildNode(separatorNode3.get());
        separatorHolder->removeChildNode(separatorNode4.get());
    }
    if (m_pageSeparator.isNull()) {
        m_pageSeparator = {int(betweenPagesOffset*pixelRatio), int(height()*pixelRatio), QImage::Format::Format_ARGB32_Premultiplied};
        m_pageSeparator.setDevicePixelRatio(pixelRatio);
        {
            QPainter painter(&m_pageSeparator);
            drawPageSeparator(&painter);
        }
    }
    auto separatorTexture = [this]() {return window()->createTextureFromImage(m_pageSeparator, {QQuickWindow::TextureCanUseAtlas,  QQuickWindow::TextureIsOpaque});}();
    QScopedPointer<QSGTexture, QScopedPointerDeleteLater> oldSeparatorTexture { static_cast<QSGImageNode *>(oldNode->firstChild()->firstChild())->texture() };
    auto const rightOf = [](const QRectF& rect) {return rect.x() + rect.width();};
    auto const setupSeparatorNode = [&](QSGImageNode *separatorNode, const QRectF& positionRect) {
        separatorNode->setRect(positionRect);
        separatorNode->setTexture(separatorTexture);
        separatorNode->setSourceRect(QRectF {0, 0, betweenPagesOffset * pixelRatio, height()*pixelRatio});
    };
#define RENDER_DEBUG 0
    auto const setupImageNode = [this, &pixelRatio](QSGImageNode *node, const QRectF& rect, CacheEntry entry) {
        node->setRect(rect);
        auto image = m_pageCache[entry];
#if RENDER_DEBUG
        auto color = [&]() {
            switch (entry) {
            case prev_prev_page:
                return QColorConstants::Svg::blue;
            case prev_page:
                return QColorConstants::Svg::green;
            case current_page:
                return QColorConstants::Svg::red;
            case next_page:
                return QColorConstants::Svg::yellow;
            case next_next_page:
                return QColorConstants::Svg::gray;
            case next_next_next_page:
                return QColorConstants::Svg::black;
            }
            Q_UNREACHABLE();
        }();
        if (true) {
            image = QImage {int(pageWidth()), int(height()), QImage::Format::Format_RGB16};
            image.fill(color);
#else
        if (image.isNull()) {
            image = QImage {int(pageWidth()), int(height()), QImage::Format::Format_RGB16};
            image.fill(Reading::mode2bgColor(m_currentColorScheme));
#endif
            image.setDevicePixelRatio(pixelRatio);
        }
        QSGTexture *texture = window()->createTextureFromImage(image, QQuickWindow::TextureIsOpaque);
        node->setTexture(texture);
        node->setSourceRect(QRectF {0, 0, pageWidth()*pixelRatio, height()*pixelRatio});
        node->setOwnsTexture(true);
    };
    const auto defaultPos = QRectF {0, 0, pageWidth(), height()};
    const auto pageSeparatorRect = QRectF {0, 0, betweenPagesOffset, height()};
    QRectF currentPagePos = defaultPos;
    {
        // draw the current page
        auto currentPageNode = static_cast<QSGImageNode *>(oldNode->childAtIndex(current));
        currentPagePos.moveLeft(pageOffset());
        setupImageNode(currentPageNode, currentPagePos, current_page);
    }
    {
        QRectF separatorNodePos = pageSeparatorRect;
        auto separatorNode = static_cast<QSGImageNode *>(oldNode->firstChild()->childAtIndex(0));
        separatorNodePos.moveRight(currentPagePos.x());
        setupSeparatorNode(separatorNode, separatorNodePos);


        //set up prevPageNode
        auto prevPageNode = static_cast<QSGImageNode *>(oldNode->childAtIndex(prev));
        auto prevPagePos = defaultPos;
        prevPagePos.moveRight(separatorNodePos.x());
        setupImageNode(prevPageNode, prevPagePos, prev_page);

        if (m_useTwoPages) {
            auto separatorNode = static_cast<QSGImageNode *>(oldNode->firstChild()->childAtIndex(2));
            QRectF separatorNodePos = pageSeparatorRect;
            separatorNodePos.moveRight(prevPagePos.x());
            setupSeparatorNode(separatorNode, separatorNodePos);

            auto prevPrevPageNode = static_cast<QSGImageNode *>(oldNode->childAtIndex(prev_prev));
            auto prevPrevPagePos = prevPagePos;
            prevPrevPagePos.moveRight(separatorNodePos.x());
            setupImageNode(prevPrevPageNode, prevPrevPagePos, prev_prev_page);
        }
    }
    {
        QRectF separatorNodePos = pageSeparatorRect;
        auto separatorNode = static_cast<QSGImageNode *>(oldNode->firstChild()->childAtIndex(1));
        separatorNodePos.moveLeft(rightOf(currentPagePos));
        setupSeparatorNode(separatorNode, separatorNodePos);

        auto nextPageNode = static_cast<QSGImageNode *>(oldNode->childAtIndex(next));
        QRectF nextPagePos = defaultPos;
        nextPagePos.moveLeft(rightOf(separatorNodePos));
        setupImageNode(nextPageNode, nextPagePos, next_page);

        if (m_useTwoPages) {
            auto separatorNode = static_cast<QSGImageNode *>(oldNode->firstChild()->childAtIndex(3));
            QRectF separatorNodePos = pageSeparatorRect;
            separatorNodePos.moveLeft(rightOf(nextPagePos));
            setupSeparatorNode(separatorNode, separatorNodePos);


            auto nextNextPageNode = static_cast<QSGImageNode *>(oldNode->childAtIndex(next_next));
            auto nextNextPagePos = defaultPos;
            nextNextPagePos.moveLeft(rightOf(separatorNodePos));
            setupImageNode(nextNextPageNode, nextNextPagePos, next_next_page);

            {
                auto separatorNode = static_cast<QSGImageNode *>(oldNode->firstChild()->childAtIndex(4));
                QRectF separatorNodePos = pageSeparatorRect;
                separatorNodePos.moveLeft(rightOf(nextNextPagePos));
                setupSeparatorNode(separatorNode, separatorNodePos);

                auto nextNextNextPageNode = static_cast<QSGImageNode *>(oldNode->childAtIndex(next_next_next));
                auto nextNextNextPagePos = defaultPos;
                nextNextNextPagePos.moveLeft(rightOf(separatorNodePos));
                setupImageNode(nextNextNextPageNode, nextNextNextPagePos, next_next_next_page);
            }
        }
    }
    return oldNode;
}

bool PageView::doesPageContainImportantPicture(int pageNumber)
{
    if (pageNumber < 0 or pageNumber > pageCount()) {
        // can happen when we render off-screen pages at the end and beginning of the text
        return false;
    }
    // assumption pageNumber is already 0 based
    QMutexLocker lock(&m_documentMutex);
    auto layout = m_textDocument.documentLayout();
    // position cursor in center of page
    // TODO: there is probably a better point than the center (and is one single point enough?)
    auto x = this->pageWidth() / 2;
    auto y = (pageNumber + 0.5) * this->height();
    auto position = layout->hitTest({x, y}, Qt::FuzzyHit);
    if (position == -1)
        position = 0;
    QTextCursor cursor {&m_textDocument};
    cursor.setPosition(position);
    auto format = cursor.charFormat();
    return format.isImageFormat();
}

QPointF PageView::translatePageViewCoordinates2DocumentCoordinates(qreal x, qreal y) const
{
    auto width = this->pageWidth();
    auto height = this->height();
    // assumption: x and y are always inside the PageView area
    if (x > width) {
        // we are on the second page, translate the x and y coordinate
        x -= width + betweenPagesOffset;
        y += (m_pageNumber - 1 /*offset for pageNumber is 1*/ + 1 /*we are on the second page*/) * height;
    } else {
        // we are on the first page, so we only need to change the y value
        y += (m_pageNumber - 1 /*offset for pageNumber is 1*/) * height;
    }
    return {x, y};
}

void PageView::handleClick(double x, double y)
{
    QMutexLocker lock(&m_documentMutex);
    auto layout = m_textDocument.documentLayout();
    auto position = layout->hitTest(translatePageViewCoordinates2DocumentCoordinates(x,y), Qt::FuzzyHit);
    if (position == -1)
        return;
    QTextCursor cursor {&m_textDocument};
    cursor.setPosition(position);
    if (cursor.charFormat().isImageFormat()) {
        return;
    }
    cursor.beginEditBlock();
    cursor.select(QTextCursor::BlockUnderCursor);
    auto oldCharFormat = cursor.charFormat();
    auto newCharFormat = QTextCharFormat {oldCharFormat};
    QBrush fgbrush { QColor { Qt::GlobalColor::darkRed } };
    newCharFormat.setForeground(fgbrush);
    QBrush bgbrush { QColor { Qt::GlobalColor::darkBlue } };
    newCharFormat.setBackground(bgbrush);
    newCharFormat.setFontItalic(true);
    cursor.setCharFormat(newCharFormat);
    cursor.endEditBlock();
    QTimer::singleShot(0, this, &PageView::asyncPopuateHelper);
    QTimer::singleShot(1000, this, [this](){
        QMutexLocker lock(&m_documentMutex);
        m_textDocument.undo();
        QTimer::singleShot(0, this, &PageView::asyncPopuateHelper);
    });
}

JNovelClubConnector *PageView::connector() const
{
    return m_connector;
}

Part PageView::currentPart() const
{
    return m_currentPart;
}

qreal PageView::initialPercentage() const
{
    return m_initialPercentage;
}

void PageView::setText(QString documentText, qreal percentage, QHash<QString, QImage> images) {
    qreal margin {};
    Q_UNUSED(percentage)
    {
        QMutexLocker lock(&m_documentMutex);
        m_URL2originalImage = images;
        margin = m_textDocument.documentMargin();
        m_textDocument.setHtml(documentText);
    }

    m_resizeTasks = images.size();
    if (m_resizeTasks == 0)
        setTextDone();
    else for (auto it = images.constBegin(); it != images.constEnd(); ++it) {
        const QImage image = it.value();
        QString imageURL = it.key();
        auto runnable = new ImageResizer(margin, image, imageURL, this);
        pool.start(runnable);
    }
}

void PageView::setTextDone() {
    {
        QMutexLocker lock(&m_documentMutex);
        m_textDocument.setHtml(m_textDocument.toHtml());
        m_pageCount = m_textDocument.pageCount();
    }
    if (!m_transitionToNextOrPrevPart)
        setPageNumber(std::max<int>(1, static_cast<int>(m_pageCount*m_initialPercentage)));
    else {
        m_transitionToNextOrPrevPart = false;
        setPageNumber(1);
    }
    emit pageCountChanged();
    emit pageNumberChanged();
    this->populateCache();
    this->update();

    emit nextPageEnabledChanged();
    emit prevPageEnabledChanged();
    emit parsingEnded();
}

void PageView::setFont(const QFont &newFont)
{
    if (m_font == newFont) {
        return;
    }

    m_font = newFont;
    QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
    settings.setValue(QStringLiteral("font"), newFont);
    emit fontChanged();
    {
        QMutexLocker lock(&m_documentMutex);
        m_textDocument.setDefaultFont(newFont);
        // inspired from https://stackoverflow.com/questions/27716625/qtextedit-change-font-of-individual-paragraph-block/27719789#27719789
        for (auto textBlock = m_textDocument.begin(); textBlock != m_textDocument.end(); textBlock = textBlock.next()) {
            if (!textBlock.isValid()) {
                qCDebug(partreader) << "invalid textblock";
                continue;
            }
            QTextCursor editingCursor(textBlock);
            if (editingCursor.isNull()) {
                qCDebug(partreader) << "isNull";
                continue;
            }
            for (auto it = textBlock.begin();
                 !it.atEnd();
                 ++it) {
                if (!it.fragment().isValid()) {
                    qCDebug(partreader) << "invalid fragment";
                    continue;
                }
                // When changing the font,one has to be careful: QTextDocument wants <em> tags to be in italics. Assigning a font which is normal or bold would lead to crashes
                auto charFormat = it.fragment().charFormat();
                QFont oldFont = charFormat.font();
                oldFont.setFamily(m_font.family());
                oldFont.setPointSizeF(m_font.pointSizeF());
                charFormat.setFont(oldFont);
                editingCursor.setPosition(it.fragment().position());
                editingCursor.setPosition(it.fragment().position() + it.fragment().length(), QTextCursor::KeepAnchor);
                editingCursor.setCharFormat(charFormat);
            }
        }
    }
    asyncPopulateCache();
}

QFont PageView::font()
{
    return m_font;
}

void PageView::resetPageOffset()
{
    m_offsetAnimation->setEndValue(0);
    m_offsetAnimation->start();
}

qreal PageView::pageOffset() const
{
    return m_pageOffset;
}

void PageView::setColorScheme(Reading::ColorScheme newColorScheme)
{
    using namespace Reading;
    //setFillColor(mode2bgColor(newColorScheme));
    if (m_currentColorScheme != newColorScheme) {
        m_currentColorScheme = newColorScheme;
        QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
        settings.setValue(QStringLiteral("colorScheme"), QMetaEnum::fromType<Reading::ColorScheme>().valueToKey(static_cast<int>(m_currentColorScheme)));
        emit colorSchemeChanged();
        asyncPopulateCache();
    }
}

void PageView::setPageOffset(qreal pageOffset)
{
    if (m_useTwoPages)
        pageOffset = qBound(2*(-pageWidth() - betweenPagesOffset) , pageOffset, 2*(pageWidth() + betweenPagesOffset));
    else
        pageOffset = qBound(-pageWidth() - betweenPagesOffset , pageOffset, pageWidth() + betweenPagesOffset);
    if (m_pageOffset != pageOffset) {
        if (!m_pageMoving || pageOffset != 0 || m_currentlyDragged) {
            m_pageOffset = pageOffset;
            this->update(); // necessary for the animation to work
        } else {
            // the page has been moved; either we are undoing the movent, or we'll go to the next page
            if (std::abs(m_pageOffset) > pageWidth()/9)
                return; // page moves, changes offset anyway
            else
                resetPageOffset();
        }
    }
}

void PageView::setConnector(JNovelClubConnector *connector)
{
    if (m_connector == connector)
        return;
    QObject::connect(connector, &JNovelClubConnector::partFetched, this, &PageView::setText);
    if (!m_connector)
        connector->fetchPart(m_currentPart, m_initialPercentage);
    m_connector = connector;
    emit connectorChanged();
}

void PageView::setCurrentPart(Part currentPart)
{
    if (m_currentPart == currentPart)
        return;

    m_currentPart = currentPart;
    emit parsingStarted();
    if (m_connector)
        m_connector->fetchPart(m_currentPart);
    emit currentPartChanged(m_currentPart);
}

void PageView::setInitialPercentage(qreal initialPercentage)
{
    if (qFuzzyCompare(m_initialPercentage, initialPercentage))
        return;

    m_initialPercentage = initialPercentage;
    emit initialPercentageChanged(m_initialPercentage);
}

void PageView::handleApplicationStateChange(Qt::ApplicationState state)
{
    if (state & Qt::ApplicationSuspended || state & Qt::ApplicationHidden || state & Qt::ApplicationInactive) {
        for (auto&& page: m_pageCache) {
            page = QImage();
        }
        m_isSuspended = true;
    } else if (m_isSuspended) {
        m_isSuspended = false;
        asyncPopulateCache();
    }
}

int PageView::colorScheme() {
    return static_cast<int>(m_currentColorScheme);
}

qreal PageView::pageWidth() const {
    return m_useTwoPages ? ((this->width() - betweenPagesOffset) / 2) : this->width();
}

bool PageView::useTwoPages() const
{
    return m_useTwoPages;
}

void PageView::setuseTwoPages(bool useTwoPages)
{
    if (useTwoPages == m_useTwoPages) {
        return;
    }
    m_useTwoPages = useTwoPages;
    if (useTwoPages) {
        m_offsetAnimation->setDuration(550);
    } else {
        m_offsetAnimation->setDuration(200);
    }
    QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
    settings.setValue(QStringLiteral("useTwoPages"), useTwoPages);
    emit useTwoPagesChanged();
    this->handleResize();

}

int PageView::pageCount() const
{
    return m_pageCount;
}

int PageView::pageNumber() const
{
    return m_pageNumber;
}

void PageView::keepScreenOn(bool enabled) {
#ifdef Q_OS_ANDROID
  QtAndroid::runOnAndroidThread([enabled]{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if (activity.isValid()) {
      QAndroidJniObject window =
          activity.callObjectMethod("getWindow", "()Landroid/view/Window;");

      if (window.isValid()) {
        const int FLAG_KEEP_SCREEN_ON = 128;
        if (enabled) {
          window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
        } else {
          window.callMethod<void>("clearFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
        }
      }
    }
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
      env->ExceptionClear();
    }
  });
#else
    // unnecessary on desktop operating systems
    // unsupported on iOS
    Q_UNUSED(enabled)
#endif
}

void PageView::gotoNextPage()
{
    const auto progress = m_useTwoPages ? 2 : 1;
    if (m_pageNumber < m_pageCount)
        setPageNumber(m_pageNumber + progress);
    else
        setPageOffset(0);
}

void PageView::gotoPrevPage()
{
    const auto progress = m_useTwoPages ? 2 : 1;
    if (isPrevPageEnabled())
        setPageNumber(m_pageNumber - progress);
    else
        setPageOffset(0);
}

void PageView::prepareSequentialPartChange() {
    m_transitionToNextOrPrevPart = true;
}

bool PageView::isPrevPageEnabled()
{
    return pageNumber() > 1;
}

bool PageView::isNextPageEnabled()
{
    return pageNumber() < pageCount();
}

bool PageView::pageMoving() {return m_pageMoving;}

void PageView::setPageMoving(bool pageMoving) {m_pageMoving = pageMoving;}

void PageView::setPageNumber(int pageNumber)
{
    pageNumber = qBound(1, pageNumber, m_pageCount);
    if (m_pageNumber == pageNumber) {
        return;
    }
    if (m_offsetAnimation->state() == QPropertyAnimation::Running) {
        m_offsetAnimation->stop();
        // FIXME: sipmly returnung is not the best idea as we theoretically loose the call
        // however, this can basically only happen if the arrow button is constantly pressed
        // or if the page move button is constantly clicked. In that case we get the update anyway
        return;
    }
    auto oldNumber = m_pageNumber;
    m_pageNumber = pageNumber;
    if (pageNumber == 1 || oldNumber == 1)
        emit prevPageEnabledChanged();
    else if (pageNumber == m_pageCount || oldNumber == m_pageCount)
        emit nextPageEnabledChanged();
    emit pageNumberChanged();
    emit percentReadChanged(m_pageNumber/ static_cast<qreal>(m_pageCount));
    const auto pageNumDifference = std::abs(oldNumber - pageNumber);
    if (pageNumDifference > 2) {
        // larger jump, do not use offset animation
        m_pageOffset = 0;
        this->asyncPopulateCache();
        return;
    }
    double animationEndValue = 0;
    auto const goPrev = pageNumber < oldNumber;
    if (goPrev) {
        animationEndValue =  pageWidth() + betweenPagesOffset;
        if (m_useTwoPages) {
            animationEndValue += betweenPagesOffset;
        }
    } else {
        animationEndValue = -pageWidth() - 2*betweenPagesOffset;
        if (m_useTwoPages) {
            animationEndValue -= betweenPagesOffset;
        }
    }
    animationEndValue *= pageNumDifference;
    m_offsetAnimation->setEndValue(animationEndValue);
    // setup additional page
    auto const connection = new QMetaObject::Connection;
    *connection = connect(m_offsetAnimation, &QPropertyAnimation::stateChanged, this, [this, connection, pageNumDifference, goPrev](QAbstractAnimation::State newState, QAbstractAnimation::State oldState) {
        Q_UNUSED(oldState)
        if (newState != QAbstractAnimation::Stopped) {
            return;
        }
        disconnect(*connection);
        delete connection;
        m_pageOffset = 0; // reset page_Offset
        // using async updates is a) not required, because the signal is already hanlded async, and b) would cause flickering
        if (goPrev) {
            this->cacheBackward(pageNumDifference);
        } else {
            this->cacheForward(pageNumDifference);
        }
    });
    m_offsetAnimation->start();
}
