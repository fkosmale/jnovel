#ifdef MANGA
#include <QThreadPool>

#include "mangaimageprovider.h"
#include "jnovelclubconnector.h"

MangaImageResponse::MangaImageResponse(const QString &id, const QSize &requestedSize, JNovelClubConnector* connector) : m_id(id), m_requestedSize(requestedSize), m_connector(connector)
{
    setAutoDelete(false);
}

QQuickTextureFactory *MangaImageResponse::textureFactory() const
{
    return QQuickTextureFactory::textureFactoryForImage(m_image);
}

void MangaImageResponse::run() {
    auto mangaID = m_id.left(24);
    auto url = m_id.mid(24);
    QImage im = m_connector->requestMangaImage(url).result();
    auto dedrm = m_connector->getDeDRMForMangaID(mangaID);
    m_image = dedrm->decrypt(im);
    if (m_requestedSize.isValid())
        m_image = m_image.scaled(m_requestedSize, Qt::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);
    emit finished();
}

MangaImageProvider::MangaImageProvider(JNovelClubConnector* connector) : m_connector(connector), m_pool{} {}

QQuickImageResponse *MangaImageProvider::requestImageResponse(const QString &id, const QSize &requestedSize)
{
    auto response = new MangaImageResponse(id, requestedSize, m_connector);
    m_pool.start(response);
    return response;
}

#endif
