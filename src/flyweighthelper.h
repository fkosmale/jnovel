#ifndef FLYWEIGHTHELPER_H
#define FLYWEIGHTHELPER_H
#include <QString>
#include <qhashfunctions.h>

// provide hash function for Boost Flyweight
inline std::size_t hash_value(QString const& s)
{
    return qHash(s);
}

// tag types for flyweight
struct authorT {};
struct illustratorOrArtistT {};
struct translatorT {};
struct editorT {};
struct thumbnailPathT {};


#endif // FLYWEIGHTHELPER_H
