#include <QJsonArray>
#include <QJsonObject>

#include "volume.h"
#include "json_helper.h"
#include <QDebug>

QString Volume::volumeID() const
{
    return m_volumeID;
}

#define X(type, name) type Volume::name() const  {return m_metaData.name();}
CommonMetadataList
#undef X

Volume Volume::fromJSON(const QJsonObject& volume, const BookType type)
{
    QJsonArray partsArray = volume[type == BookType::LightNovel ?  QStringLiteral("parts") :  QStringLiteral("mangaParts")].toArray();
    PartList parts {};
    parts.reserve(partsArray.size());
    for (const auto partGeneric: partsArray) {
        parts.push_back(Part::fromJSON(partGeneric.toObject(), type));
    }

    const auto attachments = volume[QStringLiteral("attachments")].toArray();
    const auto parsedAttachments = parseAttachments(attachments);

    return Volume {
                    volume[QStringLiteral("title")].toString(),
                    volume[QStringLiteral("titleslug")].toString(),
                    volume[QStringLiteral("titleShort")].toString(),
                    volume[QStringLiteral("titleOriginal")].toString(),
                    volume[QStringLiteral("author")].toString(),
                    volume[type == BookType::LightNovel ?  QStringLiteral("illustrator") : QStringLiteral("artist")].toString(),
                    volume[QStringLiteral("translator")].toString(),
                    volume[QStringLiteral("editor")].toString(),
                    volume[QStringLiteral("description")].toString(),
                    volume[QStringLiteral("descriptionShort")].toString(),
                    volume[QStringLiteral("tags")].toString(),
                    parsedAttachments.coverPath,
                    parsedAttachments.thumbnailPath,
                    volume[QStringLiteral("id")].toString(),
                    volume[QStringLiteral("volumeNumber")].toInt(),
                    parts
                };
}

PartList Volume::parts() const
{
    return m_parts;
}
