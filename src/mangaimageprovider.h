#ifndef MANGAIMAGEPROVIDER_H
#define MANGAIMAGEPROVIDER_H
#ifdef MANGA

#include <QQuickAsyncImageProvider>
#include <QQuickImageResponse>
#include <QUrl>
#include <QRunnable>
#include <QThreadPool>

#include <vector>

class JNovelClubConnector;

class MangaImageResponse : public QQuickImageResponse, public QRunnable
{
public:
    MangaImageResponse(QString const& id, QSize const& requestedSize, JNovelClubConnector* connector);
    QQuickTextureFactory *textureFactory() const override;
    void run() override;
private:
    QString m_id;
    QSize m_requestedSize;
    JNovelClubConnector* m_connector;
    QImage m_image;
};

class MangaImageProvider : public QQuickAsyncImageProvider
{
public:
    explicit MangaImageProvider(JNovelClubConnector* connector);
    QQuickImageResponse* requestImageResponse(QString const& id, QSize const& requestedSize) override;

private:
    JNovelClubConnector* m_connector;
    QThreadPool m_pool;
};

#endif
#endif // MANGAIMAGEPROVIDER_H
