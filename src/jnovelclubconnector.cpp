#include "jnovelclubconnector.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QSettings>
#include <QDateTime>
#include <QHostInfo>
#include <QSslConfiguration>
#include <QTimer>
#include <QNetworkCookie>
#include <QMutexLocker>
#include <QWaitCondition>
#include <QLoggingCategory>
#include <QDir>
#include <QRegularExpression>
#include <QGuiApplication>
#include <QStandardPaths>

#include <algorithm>
#ifdef Q_OS_ANDROID
#include <QtAndroidExtras>
#ifdef KEYSTORE
#include "android_keystore.h"
#endif
#endif

#include "parts.h"
#include "json_helper.h"

Q_LOGGING_CATEGORY(connector, "jnovel.connector");


JNovelClubConnector::JNovelClubConnector(QNetworkAccessManager *manager, QObject *parent)  :
    QObject(parent),
    #ifdef MANGA
    m_dedrmMutex{ },
    #endif
    m_manager(manager),
    m_settings(new QSettings(QStringLiteral("J-novel.club"),
                             QStringLiteral("reader"), this)),
    m_currentSeriesID(QLatin1String("")),
    m_accessToken(QLatin1String("")),
    m_userid(m_settings->value(QStringLiteral("userID"), "").toString()),
    m_lastFetchedPart({})
{
    QHostInfo::lookupHost(QStringLiteral("api.j-novel.club"), [](auto info){(void) info;});
    // ensure that HTTP/2 is used when possible
    auto sslConfig = QSslConfiguration::defaultConfiguration();
    auto allowed = sslConfig.allowedNextProtocols();
    allowed.push_front(QSslConfiguration::ALPNProtocolHTTP2);
    sslConfig.setAllowedNextProtocols(allowed);
    manager->connectToHostEncrypted(QStringLiteral("api.j-novel.club"), 443);
}

void JNovelClubConnector::handleNetworkError(QNetworkReply* reply) {
    qCWarning(connector) << reply->errorString();
}

#ifdef MANGA
void JNovelClubConnector::fetchMangaImageURLs(QString mangaPartID)
{
    auto queryString = QStringLiteral("https://api.j-novel.club/api/mangaParts/%1/token").arg(mangaPartID);
    QUrl getMangaToken {queryString};
    QNetworkRequest request(getMangaToken);
    request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::AlwaysNetwork);
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply, mangaPartID](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isObject());
        auto infoObject = parsedJSON.object();
        MangaTokenInfo info {
            infoObject["uuid"].toString(),
            infoObject["ngtoken"].toString(),
            infoObject["kgtoken"].toString()
        };
        auto queryString = QStringLiteral("https://m11.j-novel.club/nebel/wp/%1").arg(info.uuid);
        QUrl getMangaStructure{queryString};
        QNetworkRequest request(getMangaStructure);
        request.setHeader(QNetworkRequest::ContentTypeHeader,
            "application/x-www-form-urlencoded");
        request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
        QNetworkReply* reply = m_manager->post(request,info.ngtoken.toUtf8());
        QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply, mangaPartID](){
            reply->deleteLater();
            if (reply->error()) {
                this->handleNetworkError(reply);
                return;
            }
            auto data = reply->readAll();
            auto parsedJSON = QJsonDocument::fromJson(data);
            Q_ASSERT(parsedJSON.isObject());
            auto webpub = parsedJSON.object();
            auto readingOrder = webpub["readingOrder"];
            Q_ASSERT(readingOrder.isArray());
            auto readingOrderList = readingOrder.toArray();
            std::vector<QUrl> imageURLList;
            imageURLList.reserve( static_cast<decltype (imageURLList)::size_type>(readingOrderList.size()));
            std::transform(readingOrderList.constBegin(), readingOrderList.constEnd(), std::back_inserter(imageURLList), [](const auto& listEntry) {
                return QUrl{ listEntry.toObject()["href"].toString() };
           });
           emit fetchedMangaImageLinks(mangaPartID, imageURLList);
        });
    });
}
#endif

void JNovelClubConnector::fetchLNSeriesList()
{
    emit LNSeriesFetched(SeriesList {});
    QUrl getAllSeriesURL(QStringLiteral("https://api.j-novel.club/api/series"));
    QNetworkRequest request(getAllSeriesURL);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    QNetworkReply* reply = m_manager->get(request);

    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto seriesListJSON = parsedJSON.array();
        SeriesList seriesList = SeriesList();
        seriesList.reserve(seriesListJSON.size());
        std::transform(seriesListJSON.constBegin(), seriesListJSON.constEnd(), std::back_inserter(seriesList), [](const auto& seriesGeneric) {
            auto series = seriesGeneric.toObject();
            return Series::fromJSON(series);
        });
        emit LNSeriesFetched(seriesList);
    });
}

#ifdef MANGA
void JNovelClubConnector::fetchMangaSeriesList()
{
    emit MangaSeriesFetched(SeriesList {});
    QUrl getAllSeriesURL(QStringLiteral("https://api.j-novel.club/api/mangaseries"));
    QNetworkRequest request(getAllSeriesURL);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    QNetworkReply* reply = m_manager->get(request);

    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto seriesListJSON = parsedJSON.array();
        SeriesList seriesList = SeriesList();
        seriesList.reserve(seriesListJSON.size());
        std::transform(seriesListJSON.constBegin(), seriesListJSON.constEnd(), std::back_inserter(seriesList), [](const auto& seriesGeneric) {
            auto series = seriesGeneric.toObject();
            return Series::fromJSON(series, BookType::Manga);
        });
        emit MangaSeriesFetched(seriesList);
    });
}
#endif

void JNovelClubConnector::fetchLNVolumesForID(const QString& seriesID)
{
    m_currentSeriesID = seriesID;
    auto queryString = QStringLiteral("https://api.j-novel.club/api/volumes?filter[where][serieId]=%1&filter[include][parts]").arg(seriesID);
    QUrl getVolumesWithParts(queryString);
    QNetworkRequest request(getVolumesWithParts);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    emit LNVolumesFetched({});
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto volumesJSON = parsedJSON.array();
        VolumeList volumes {};
        volumes.reserve(volumesJSON.size());
        for (const auto volumeGeneric: volumesJSON) {
            auto volume = volumeGeneric.toObject();
            volumes.push_back(Volume::fromJSON(volume));
        }
        emit LNVolumesFetched(volumes);
    });
}

#ifdef MANGA
void JNovelClubConnector::fetchMangaVolumesForID(const QString& seriesID)
{
    m_currentSeriesID = seriesID;
    auto queryString = QStringLiteral("https://api.j-novel.club/api/mangaVolumes?filter[where][mangaSerieId]=%1&filter[include][mangaParts]").arg(seriesID);
    QUrl getVolumesWithParts(queryString);
    QNetworkRequest request(getVolumesWithParts);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto volumesJSON = parsedJSON.array();
        VolumeList volumes {};
        volumes.reserve(volumesJSON.size());
        for (const auto volumeGeneric: volumesJSON) {
            auto volume = volumeGeneric.toObject();
            volumes.push_back(Volume::fromJSON(volume, BookType::Manga));
        }
        emit MangaVolumesFetched(volumes);
    });
}
#endif

void JNovelClubConnector::fetchPart(const Part part, qreal percentageRead, bool storeOffline)
{
    auto partId = part.partID();
    auto cachePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    QDir cacheDir(cachePath);
    QFile file(cacheDir.filePath(QLatin1String("JNOVEL")+partId));
    if (file.exists() && file.open(QIODevice::ReadOnly)) {
        QDataStream in(&file);
        QString html;
        QHash<QString, QImage> url2image;
        in >> html;
        in >> url2image;
        emit partFetched(html, percentageRead, url2image);
        return;
    }
    const auto queryString = QStringLiteral("https://api.j-novel.club/api/parts/%1/partData").arg(partId);
    m_lastFetchedPart = part;
    emit lastFetchedPartChanged();
    QUrl getPart(queryString);
    QNetworkRequest request(getPart);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, partId, reply, percentageRead, storeOffline]{
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJson = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJson.isObject());
        auto partText = parsedJson.object()[QStringLiteral("dataHTML")].toString();
        parseNovelString(partText, percentageRead, partId, storeOffline);
    });
}

inline QString makeAlias(const QString &service, const QString &key)
{
    return service + QLatin1Char('/') + key;
}

void JNovelClubConnector::encryptAndStorePassword(QString const& pass) {
#ifdef Q_OS_ANDROID
#ifdef KEYSTORE
    using android::content::Context;
    using android::security::KeyPairGeneratorSpec;
    using java::io::ByteArrayOutputStream;
    using java::security::interfaces::RSAPublicKey;
    using java::security::KeyPairGenerator;
    using java::security::KeyStore;
    using java::util::Calendar;
    using javax::crypto::Cipher;
    using javax::crypto::CipherOutputStream;
    using javax::security::auth::x500::X500Principal;

    const KeyStore keyStore = KeyStore::getInstance(QStringLiteral("AndroidKeyStore"));

        if (!keyStore || !keyStore.load()) {
            // handle error
            return;
        }

        auto const key = QString {};
        auto const &alias = makeAlias("JNovelReader", key);
        if (!keyStore.containsAlias(alias)) {
            const Calendar start = Calendar::getInstance();
            const Calendar end = Calendar::getInstance();
            end.add(Calendar::YEAR, 99);

            const KeyPairGeneratorSpec spec =
                    KeyPairGeneratorSpec::Builder(Context(QtAndroid::androidActivity())).
                    setAlias(alias).
                    setSubject(X500Principal(QStringLiteral("CN=QtKeychain, O=Android Authority"))).
                    setSerialNumber(java::math::BigInteger::ONE).
                    setStartDate(start.getTime()).
                    setEndDate(end.getTime()).
                    build();

            const KeyPairGenerator generator = KeyPairGenerator::getInstance(QStringLiteral("RSA"),
                                                                             QStringLiteral("AndroidKeyStore"));

            if (!generator) {
                // handle error
                return;
            }

            generator.initialize(spec);

            if (!generator.generateKeyPair()) {
                // handle error
                return;
            }
        }

        const KeyStore::PrivateKeyEntry entry = keyStore.getEntry(alias);

        if (!entry) {
            // handle error
            return;
        }

        const RSAPublicKey publicKey = entry.getCertificate().getPublicKey();
        const Cipher cipher = Cipher::getInstance(QStringLiteral("RSA/ECB/PKCS1Padding"),
                                                  QStringLiteral("AndroidOpenSSL"));

        if (!cipher || !cipher.init(Cipher::ENCRYPT_MODE, publicKey)) {
            // handle error
            return;
        }

        ByteArrayOutputStream outputStream;
        CipherOutputStream cipherOutputStream(outputStream, cipher);

        auto const data = pass.toLocal8Bit();
        if (!cipherOutputStream.write(data) || !cipherOutputStream.close()) {
            // handle error
            return;
        }

        m_settings->setValue("encryptedPassword", outputStream.toByteArray());
#endif
#else
    Q_UNUSED(pass);
#endif
}

bool JNovelClubConnector::checkIfLoggedIn()
{
    if (loggedIn()) {
        return true;
    }
    auto variant = m_settings->value(QStringLiteral("expirationDate"));
    if (variant.isValid() && QDateTime::fromString(variant.toString(), Qt::ISODateWithMs) > QDateTime::currentDateTime()) {
        m_accessToken = m_settings->value(QStringLiteral("accessToken")).toString();
        m_loginStatus = LoggedIn;
        emit loginSuccess();
        emit loggedInChanged();
        emit loginStatusChanged(m_loginStatus);
        // renew one day before it lapses
        auto renewalTime = qMin(0ll, QDateTime::currentDateTime().msecsTo(QDateTime::fromString(variant.toString(), Qt::ISODateWithMs).addDays(-1)));
        QTimer::singleShot(renewalTime, this, &JNovelClubConnector::loginWithStoredCredentials);
        return true;
    } else {
        m_settings->remove(QStringLiteral("accessToken"));
        m_settings->remove(QStringLiteral("expirationDate"));
        m_loginStatus = LoggedOff;
        emit loginStatusChanged(m_loginStatus);
        emit loggedInChanged();
        loginWithStoredCredentials();
    }    
    #ifdef Q_OS_ANDROID
    #ifdef KEYSTORE
        auto encryptedData = QByteArray {};
        using android::content::Context;
        using android::security::KeyPairGeneratorSpec;
        using java::io::ByteArrayInputStream;
        using java::security::interfaces::RSAPublicKey;
        using java::security::KeyPair;
        using java::security::KeyPairGenerator;
        using java::security::KeyStore;
        using java::util::Calendar;
        using javax::crypto::Cipher;
        using javax::crypto::CipherInputStream;
        using javax::security::auth::x500::X500Principal;

        auto const keyStore = KeyStore::getInstance(QStringLiteral("AndroidKeyStore"));
        if (!keyStore || !keyStore.load()) {
            // handle error?
        }

        QString key {};
        auto const &alias = makeAlias(QLatin1String("JNovelReader"), key);
        const KeyStore::PrivateKeyEntry entry = keyStore.getEntry(alias);
        if (!entry) {
            // handle error
        }

        auto const cipher = Cipher::getInstance(QStringLiteral("RSA/ECB/PKCS1Padding"),
                                                QStringLiteral("AndroidOpenSSL"));

        if (!cipher || !cipher.init(Cipher::DECRYPT_MODE, entry.getPrivateKey())) {
            // handle error
        }
        QByteArray plainData;
        CipherInputStream const inputStream(ByteArrayInputStream(encryptedData), cipher);

        for (int nextByte; (nextByte = inputStream.read()) != -1; ) {
            plainData.append(nextByte);
        }
        auto const email = m_settings->value(QStringLiteral("email")).toString();
        if (!email.isEmpty()) {
            auto const pass = QString { plainData };
            login(email, pass);
        }
    #endif
    #endif
    return false;
}

QString JNovelClubConnector::createAccessCookiesString()
{
    auto const expirationDate = QDateTime::fromString(m_settings->value(QStringLiteral("expirationDate")).toString(), Qt::ISODateWithMs);
    auto accessCookie = QNetworkCookie { "access_token", m_settings->value(QStringLiteral("accessTokenValue")).toByteArray()};
    accessCookie.setDomain(QLatin1String(".j-novel.club"));
    accessCookie.setPath(QLatin1String("/"));
    accessCookie.setExpirationDate(expirationDate);
    auto userCookie = QNetworkCookie { "userId", m_settings->value(QStringLiteral("userIdValue")).toByteArray()};
    userCookie.setDomain(QLatin1String(".j-novel.club"));
    userCookie.setPath(QLatin1String("/"));
    userCookie.setExpirationDate(expirationDate);
    QString ret {accessCookie.toRawForm()};
    return ret + userCookie.toRawForm();
}

void JNovelClubConnector::parseNovelString(const QString &part, qreal percentage, QString partId, bool storeOffline)
{
    QString wellFormedHtml = QStringLiteral("<!DOCTYPE html><html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"></head><body>%1</body></html>").arg(part);

    // extract all image URLs (the HTML should be simple enough in this case to make the RegEx approach feasible
    static QRegularExpression re(QStringLiteral(R"|(<img.*src="([^"]*)".*(/?)>)|"));
    re.optimize();
    QRegularExpressionMatchIterator it = re.globalMatch(wellFormedHtml);
    boost::container::small_vector<QString, 4> imageURLS {}; // most parts do not have many images, so we use a small_vector with a small capacity
    while (it.hasNext()) {
        imageURLS.push_back(it.next().captured(1));
    }
    // wrap img tags in p tags (cannot use capture group 0 in replace)
    // and replace centerp with align=center
    QRegularExpression re_centerp(QStringLiteral(R"|(<p class="centerp">)|"));
    QRegularExpression re_normalp(QLatin1String("<p>"));
    auto fixedUpDocument = wellFormedHtml
            .replace(re, QLatin1String(R"|(<p align="center" class="image"><img src="\1"></p>)|"))
            .replace(re_centerp, QLatin1String(R"|(<p align="center">)|"))
            .replace(re_normalp, QLatin1String(R"|(<p align="justify">)|"));

    auto imageReplyHandler = new ImageReplyHandler(std::move(wellFormedHtml), percentage, imageURLS.size(), this, partId, storeOffline);
    for (const auto& imageURL: qAsConst(imageURLS)) {
        QNetworkRequest req(QUrl {imageURL});
        QNetworkReply* reply = m_manager->get(req);
        QObject::connect(reply, &QNetworkReply::finished, imageReplyHandler, &ImageReplyHandler::handleReply);
    }
    if (imageURLS.empty())
        imageReplyHandler->tryComplete();
}

void JNovelClubConnector::storeOffline(QString partId, QString html, QHash<QString, QImage> url2image)
{
    auto cachePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    QDir cacheDir(cachePath);
    QFile file(cacheDir.filePath(QLatin1String("JNOVEL")+partId));
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out << html << url2image;
}


#ifdef MANGA
void JNovelClubConnector::getDecoderForImageURLs(QString mangaPartID, std::vector<QUrl> urls, bool saveImages)
{
    // reimplement this at a later point
}
#endif

void JNovelClubConnector::fetchUserData()
{
    const auto queryString = QStringLiteral("https://api.j-novel.club/api/users/%1?filter={\"include\":[{\"ownedBooks\":\"serie\"},\"readParts\",\"roles\"]}").arg(m_userid);
    QUrl getUserData(queryString);
    QNetworkRequest request(getUserData);
    request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply]{
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJson = QJsonDocument::fromJson(data);
        auto user = User::fromJSON(parsedJson.object());
        emit fetchedUserData(user);
    });
}

void JNovelClubConnector::refreshVolumes()
{
    fetchLNVolumesForID(m_currentSeriesID);
}

void JNovelClubConnector::loginWithStoredCredentials()
{
    auto email = m_settings->value(QStringLiteral("emailAddress")).toString();
    auto password = m_settings->value(QStringLiteral("password")).toString();
    if (! (email.isEmpty() || password.isEmpty() ) )
        login(email, password);
}

void JNovelClubConnector::login(const QString& email, const QString& pass)
{
    m_loginStatus = Pending;
    emit loginStatusChanged(m_loginStatus);
    auto queryString = QStringLiteral("https://api.j-novel.club/api/users/login?include=user");
    QUrl logMeIn(queryString);
    QNetworkRequest request(logMeIn);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject payload;
    payload.insert(QStringLiteral("email"), email);
    payload.insert(QStringLiteral("password"), pass);

    QJsonDocument document;
    document.setObject(payload);


    QNetworkReply* reply = m_manager->post(request, document.toJson(QJsonDocument::Compact));


    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        switch (reply->error()) {
            case QNetworkReply::NoError:
                break;
            default:
                m_loginStatus = Failed;
                emit loginStatusChanged(m_loginStatus);
                emit loginFailed(QStringLiteral("Login failed!"));
        }
        QVariant cookieVariant = reply->header(QNetworkRequest::SetCookieHeader);
        if (cookieVariant.isValid()) {
            auto cookies = cookieVariant.value<QList<QNetworkCookie>>();
            const auto authCookieIt = std::find_if(cookies.constBegin(), cookies.constEnd(), [](const auto& cookie){
                return cookie.name() == "access_token";
            });
            const auto userCookieIt = std::find_if(cookies.constBegin(), cookies.constEnd(), [](const auto& cookie){
                return cookie.name() == "userId";
            });
            if (authCookieIt != cookies.constEnd()) {
                const auto& authCookie = *authCookieIt;
                const auto& userCookie = (userCookieIt != cookies.constEnd()) ? *userCookieIt : *authCookieIt; // use some default value for the unlikely case that the cookie does not exist
                auto expiresOn = authCookie.expirationDate();
                auto data = reply->readAll();
                auto json = QJsonDocument::fromJson( data);
                m_userid = json.object()[QStringLiteral("userId")].toString();
                m_accessToken = QString::fromUtf8(authCookie.value()).mid(4, 64);
                m_settings->setValue(QStringLiteral("expirationDate"), expiresOn.toString(Qt::ISODateWithMs));
                m_settings->setValue(QStringLiteral("accessToken"), m_accessToken);
                m_settings->setValue(QStringLiteral("userIdValue"), userCookie.value());
                m_settings->setValue(QStringLiteral("accessTokenValue"), authCookie.value());
                m_settings->setValue(QStringLiteral("userID"), m_userid);
                m_loginStatus = LoggedIn;
                emit loginStatusChanged(m_loginStatus);
                emit loginSuccess();
                emit loggedInChanged();
            }
        }
    });
}

void JNovelClubConnector::updatePartCompletionStatus(const Part& part, qreal readPercentage)
{
    if (!loggedIn()) {
        return;
    }
    auto queryString = QStringLiteral("https://api.j-novel.club/api/users/%1/updateReadCompletion");
    QUrl updateReadCompletion(queryString.arg(m_userid));
    QNetworkRequest request(updateReadCompletion);
    request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject payload;
    payload.insert(QStringLiteral("partId"), part.partID());
    payload.insert(QStringLiteral("completion"), readPercentage);

    QJsonDocument document;
    document.setObject(payload);


    QNetworkReply* reply = m_manager->post(request, document.toJson(QJsonDocument::Compact));

    QObject::connect(reply, &QNetworkReply::finished, reply, [reply](){
        reply->deleteLater();
    });
}

void JNovelClubConnector::setAccessToken(const QString &accessToken)
{
    m_accessToken = accessToken;
    emit loggedInChanged();
}

bool JNovelClubConnector::loggedIn()
{
    return !m_accessToken.isEmpty();
}

int JNovelClubConnector::loginStatus() const
{
    return m_loginStatus;
}

QFuture<QImage> JNovelClubConnector::requestMangaImage(QString const & url)
{
    // reimplement this at a later point
    QFuture<QImage> ret;
    return ret;
}

#ifdef MANGA
DeDRM *JNovelClubConnector::getDeDRMForMangaID(const QString &id)
{
    QMutexLocker lock(&m_dedrmMutex);
    decltype(m_mangaID2dedrm.begin()) it;
    while( (it = m_mangaID2dedrm.find(id) ) == m_mangaID2dedrm.end()) {
        m_dedrmExists.wait(&m_dedrmMutex);
    }
    Q_ASSERT(it->second);
    return it->second.get();
}
#endif //MANGA

void JNovelClubConnector::fetchLatestReleasedParts(int offset, unsigned limit)
{
    // TODO: there's still a race condition when the network reply for a later request arrives before the finish of a previous one
    if (offset == -1)
        offset = m_alreadyFetchedCount;
    // hack to reset model
    bool resetModel = false;
    if (offset == -2 ) {
        offset = 0;
        m_alreadyFetchedCount = 0;
        resetModel = true;
    }
    m_alreadyFetchedCount += limit;
    auto queryString = QStringLiteral ("https://api.j-novel.club/api/parts");
    QUrl requsetLatestParts(queryString);
    // ?filter[order]=launchDate%%20DESC&filter[limit]=%d&filter[offset]=%d
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("filter[order][0]"), QStringLiteral("launchDate DESC"));
    query.addQueryItem(QStringLiteral("filter[order][1]"), QStringLiteral("title DESC"));
    query.addQueryItem(QStringLiteral("filter[offset]"), QString::number(offset));
    query.addQueryItem(QStringLiteral("filter[limit]"), QString::number(limit));
    requsetLatestParts.setQuery(query);

    QNetworkRequest request(requsetLatestParts);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, this, [this, reply, resetModel](){
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJson = QJsonDocument::fromJson(data);

        //Q_ASSERT(parsedJson.isArray());
        auto partsArray = parsedJson.array();
        PartList parts {};
        parts.reserve(partsArray.size());
        for (const auto partGeneric: partsArray) {
            parts.push_back(Part::fromJSON(partGeneric.toObject(), BookType::LightNovel));
        }
        if (resetModel)
            emit resetLatestReleasedParts(parts);
        else
            emit fetchedLatestReleasedParts(parts);
    });
}



ImageReplyHandler::ImageReplyHandler(QString wellformedHtml, qreal percentage, int imageCount, JNovelClubConnector *connector, QString partId, bool storeOffline)
    : m_wellformedHtml(std::move(wellformedHtml))
    , m_partId(partId)
    , m_percentage(percentage)
    , m_connector(connector)
    , m_remainingImageCount(imageCount)
    , m_storeOffline(storeOffline) {}

void ImageReplyHandler::tryComplete()
{
    if (m_remainingImageCount == 0) {
        this->deleteLater();
        if (!m_storeOffline) {
            emit m_connector->partFetched(m_wellformedHtml, m_percentage, m_images);
        } else {
            m_connector->storeOffline(m_partId, m_wellformedHtml, m_images);
        }
    }
}

void ImageReplyHandler::handleReply() {
    auto reply = static_cast<QNetworkReply*>(QObject::sender());
    reply->deleteLater();
    if (reply->error()) {
        // ensure that broken images don't crash the reader
        m_connector->handleNetworkError(reply);
    } else {
        auto dpr = qGuiApp->devicePixelRatio();
        QImage image;
        image.loadFromData(reply->readAll());
        image.setDevicePixelRatio(dpr);
        m_images.insert(reply->url().toString(), std::move(image));
    }
    --m_remainingImageCount;
    tryComplete();
}
