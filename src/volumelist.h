#ifndef VOLUMELIST_H
#define VOLUMELIST_H

#include <boost/container/vector.hpp>
#include "volume.h"

using VolumeList = boost::container::vector<Volume>;

#endif // VOLUMELIST_H
