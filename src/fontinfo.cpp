#include "fontinfo.h"

FontInfo::FontInfo(QObject *parent) : QObject(parent)
{

}

QList<int> FontInfo::getFontSizes(const QFont &font)
{
    QList<int> sizes {m_fdb.smoothSizes(font.family(), m_fdb.styleString(font))};
    if (sizes.isEmpty())
        sizes = m_fdb.pointSizes(font.family(), m_fdb.styleString(font));
    return sizes.isEmpty() ? m_fdb.standardSizes() : sizes;
}
