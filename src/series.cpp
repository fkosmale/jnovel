#include <QJsonArray>
#include <QJsonObject>

#include "series.h"
#include "json_helper.h"

Series Series::fromJSON(const QJsonObject& series, const BookType type) {
    const auto parsedAttachments = parseAttachments(series[QStringLiteral("attachments")].toArray());

    return Series {
                    series[QStringLiteral("title")].toString(),
                    series[QStringLiteral("titleShort")].toString(),
                    series[QStringLiteral("titleOriginal")].toString(),
                    series[QStringLiteral("titleslug")].toString(),
                    series[QStringLiteral("author")].toString(),
                    series[type == BookType::LightNovel ?  QStringLiteral("illustrator") : QStringLiteral("artist")].toString(),
                    series[QStringLiteral("translator")].toString(),
                    series[QStringLiteral("editor")].toString(),
                    series[QStringLiteral("description")].toString(),
                    series[QStringLiteral("descriptionShort")].toString(),
                    series[QStringLiteral("tags")].toString(),
                    parsedAttachments.coverPath,
                    parsedAttachments.thumbnailPath,
                    series[QStringLiteral("id")].toString()
                };
}

#define X(type, name) type Series::name() const  {return m_metaData.name();}
CommonMetadataList
#undef X

QString Series::seriesID() const
{
    return m_seriesID;
}
