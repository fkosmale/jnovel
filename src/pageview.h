#ifndef PAGEVIEW_H
#define PAGEVIEW_H

#include <QQuickPaintedItem>
#include <QImage>
#include <QHash>
#include <QFont>
#include <QPixmap>
#include <QTextDocument>
#include <array>
#include <QMutex>
#include <QLoggingCategory>
#include <QThreadPool>
#include "parts.h"

#include "readingmode.h"

Q_DECLARE_LOGGING_CATEGORY(partreader)


class QPainter;
class QScreen;
class QSettings;
class QPropertyAnimation;

using PageCache = std::array<QImage, 6>;

using CacheEntry = PageCache::size_type;

class JNovelClubConnector;

class PageView : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(PageView)

    friend struct ImageResizer;

public:
    Q_PROPERTY(int pageNumber READ pageNumber WRITE setPageNumber NOTIFY pageNumberChanged)
    Q_PROPERTY(int pageCount READ pageCount NOTIFY pageCountChanged)
    Q_PROPERTY(bool useTwoPages READ useTwoPages WRITE setuseTwoPages NOTIFY useTwoPagesChanged)
    Q_PROPERTY(int colorScheme READ colorScheme NOTIFY colorSchemeChanged)
    Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(qreal pageOffset READ pageOffset WRITE setPageOffset NOTIFY pageOffsetChanged)
    Q_PROPERTY(bool prevPageEnabled READ isPrevPageEnabled NOTIFY prevPageEnabledChanged)
    Q_PROPERTY(bool nextPageEnabled READ isNextPageEnabled NOTIFY nextPageEnabledChanged)
    Q_PROPERTY(bool pageMoving READ pageMoving WRITE setPageMoving NOTIFY nextPageEnabledChanged)
    Q_PROPERTY(bool currentlyDragged MEMBER m_currentlyDragged)
    Q_PROPERTY(JNovelClubConnector* connector READ connector WRITE setConnector NOTIFY connectorChanged)
    Q_PROPERTY(Part currentPart READ currentPart WRITE setCurrentPart NOTIFY currentPartChanged)
    Q_PROPERTY(qreal initialPercentage READ initialPercentage WRITE setInitialPercentage NOTIFY initialPercentageChanged)


protected:
    virtual QSGNode* updatePaintNode(QSGNode* oldNode, UpdatePaintNodeData* updatePaintNodeData) override;

public:
    PageView(QQuickItem *parent = nullptr);
    int pageNumber() const;
    void setPageNumber(int pageNumber);
    int pageCount() const;
    bool useTwoPages() const;
    void setuseTwoPages(bool useTwoPages);
    void setFont(const QFont& newFont);
    QFont font();
    Q_INVOKABLE void resetPageOffset();
    Q_INVOKABLE void keepScreenOn(bool enabled);
    Q_INVOKABLE void gotoNextPage();
    Q_INVOKABLE void gotoPrevPage();
    Q_INVOKABLE void prepareSequentialPartChange();
    bool isPrevPageEnabled();
    bool isNextPageEnabled();
    bool pageMoving();
    void setPageMoving(bool pageMoving);
    qreal pageOffset() const;
    Q_INVOKABLE void handleClick(double x, double y);

    JNovelClubConnector* connector() const;
    QThreadPool pool;

    Part currentPart() const;

    qreal initialPercentage() const;

signals:
    void parsingStarted();
    void parsingEnded();
    void pageNumberChanged();
    void pageCountChanged();
    void useTwoPagesChanged();
    void colorSchemeChanged();
    void fontChanged();
    void percentReadChanged(qreal percentage);
    void pageOffsetChanged(qreal offset); // actually never emitted as there is noone listening to it
    void nextPageEnabledChanged();
    void prevPageEnabledChanged();
    void connectorChanged();

    void currentPartChanged(Part currentPart);

    void initialPercentageChanged(qreal initialPercentage);

public slots:
    void setText(QString documentText, qreal percentage, QHash<QString, QImage> images);
    void setColorScheme(Reading::ColorScheme readingMode);
    // stupid overload of setColorScheme because QML doesn't manage to pass the enum class to the function
    void setColorScheme(int colorScheme) {setColorScheme(static_cast<Reading::ColorScheme>(colorScheme));}
    void setPageOffset(qreal pageOffset);
    void setConnector(JNovelClubConnector* connector);

    void setCurrentPart(Part currentPart);

    void setInitialPercentage(qreal initialPercentage);

    void handleApplicationStateChange(Qt::ApplicationState state);

private slots:
    void setTextDone();

private:

    JNovelClubConnector* m_connector = nullptr;
    QHash<QString, QImage> m_URL2originalImage;
    QTextDocument m_textDocument;
    QMutex m_documentMutex;
    QPropertyAnimation* m_offsetAnimation;
    QFont m_font;
    PageCache m_pageCache;
    QImage m_pageSeparator;
    qreal m_pageOffset;
    qreal m_initialPercentage = 0;
    Part m_currentPart;
    int m_textPositionAtTopOfPage;
    int m_pageNumber;
    int m_pageCount;
    int m_resizeTasks = 0;
    Reading::ColorScheme m_currentColorScheme = Reading::ColorScheme::DARK;
    bool m_useTwoPages:1 = true;
    bool m_pageMoving:1 = false;
    bool m_isSuspended:1 = false;
    bool m_transitionToNextOrPrevPart:1 = false;
    bool m_currentlyDragged: 1 = false;

    int colorScheme();
    qreal pageWidth() const;
    void handleResize();
    void handleResizeDone();
    void setupPainter(QPainter* painter);
    void renderPage(QPainter* painter, int pageNumber);
    void populateCache();
    void asyncPopulateCache();
    void cacheForward(int dist);
    void cacheBackward(int dist);
    void addCacheEntry(const CacheEntry entry, const int page);
    void drawPageSeparator(QPainter* painter);
    void asyncPopuateHelper(void);
    bool doesPageContainImportantPicture(int pageNumber);
    QPointF translatePageViewCoordinates2DocumentCoordinates(qreal x, qreal y) const;
};

#endif // PAGEVIEW_H
