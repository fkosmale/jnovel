#include "mangaimagesmodel.h"

MangaImagesModel::MangaImagesModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int MangaImagesModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return (m_imagePaths.size() + 1) / 2;
}

QVariant MangaImagesModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.isValid());

    auto real_row = index.row()  * 2;
    switch (role) {
    case Qt::DisplayRole:
        if (real_row >= m_imagePaths.size())
            return QVariant {};
        return QVariant::fromValue(m_imagePaths.at(real_row));
    case PrevPageRole:
        if (real_row - 1 >= 0)
            return QVariant::fromValue(m_imagePaths.at(real_row - 1));
        else {
            break;
        }
    case HasPrevPageRole:
        return QVariant::fromValue(real_row - 1 >= 0);
    }

    return QVariant {};
}

void MangaImagesModel::setImagePath(std::vector<QString> imagePaths)
{
    beginResetModel();
    m_imagePaths = std::move(imagePaths);
    endResetModel();
}

QHash<int, QByteArray> MangaImagesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Qt::DisplayRole] = "display";
    roles[PrevPageRole] = "prevPage";
    roles[HasPrevPageRole] = "hasPrevPage";
    return roles;
}
