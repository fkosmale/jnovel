#ifndef READINGMODE_H
#define READINGMODE_H

#include <qobjectdefs.h>
#include <QColor>

namespace Reading
{    
    Q_NAMESPACE
    
    enum class ColorScheme { BLACK=3, LIGHT=2, DARK=1, SEPIA=0 };
    Q_ENUM_NS(ColorScheme)
    
    constexpr QColor mode2bgColor(ColorScheme mode)
    {
        switch (mode) {
            case ColorScheme::BLACK:
                return QColor(0, 0, 0);
            case ColorScheme::DARK:
                return QColor(51, 51, 51);
            case ColorScheme::LIGHT:
                return QColor(255, 255, 255);
            case ColorScheme::SEPIA:
                return QColor(244, 236, 216);
        }
        Q_UNREACHABLE();
    }

    constexpr QColor mode2fgColor(ColorScheme mode)
    {
        switch (mode) {
            case ColorScheme::BLACK:
                return QColor(252, 252, 252);
            case ColorScheme::DARK:
                return QColor(238,238,238);
            case ColorScheme::LIGHT:
                return QColor(51,51,51);
            case ColorScheme::SEPIA:
                return QColor(91, 70, 54);
        }
        Q_UNREACHABLE();
    }
}


#endif
