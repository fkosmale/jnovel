#ifndef JNOVELCLUBCONNECTOR_H
#define JNOVELCLUBCONNECTOR_H

#include <QObject>
#include <QString>
#include <QFuture>
#include <QImage>
#include <QMutex>
#include <QWaitCondition>

#include <map>

#include "serieslist.h"
#include "volumelist.h"
#include "user.h"
#include "parts.h"
#include "partlist.h"
#include "readingmode.h"

class QNetworkAccessManager;
class QSettings;
class QNetworkReply;

template<class T>
using observer_ptr = T*;

struct MangaTokenInfo {
    QString uuid;
    QString ngtoken;
    QString kgtoken;
};

class JNovelClubConnector : public QObject
{
    Q_OBJECT

    Q_DISABLE_COPY(JNovelClubConnector)

    Q_PROPERTY(bool loggedIn READ loggedIn NOTIFY loggedInChanged)
    Q_PROPERTY(Part lastFetchedPart MEMBER m_lastFetchedPart NOTIFY lastFetchedPartChanged)
    Q_PROPERTY(int loginStatus READ loginStatus NOTIFY loginStatusChanged)

    enum LoginStatus {
        LoggedOff = 0,
        Pending = 1,
        Failed = 2,
        LoggedIn = 3
    };
    friend struct ImageReplyHandler;

public:
    explicit JNovelClubConnector(QNetworkAccessManager *manager, QObject *parent = nullptr);

    void setAccessToken(const QString &accessToken);
    bool loggedIn();
    int loginStatus() const;
    QFuture<QImage> requestMangaImage(QString const & url);
    // can block
#ifdef MANGA
    DeDRM* getDeDRMForMangaID(QString const& id);
#endif

signals:
    // user related signals
    void loginStatusChanged(int loginStatus);
    void loginSuccess();
    void loginFailed(const QString& errorMessage);
    void loggedInChanged();
    void fetchedUserData(const User& user);

    // LN related signals
    void LNSeriesFetched(const SeriesList& series);
    void LNVolumesFetched(const VolumeList& volumes);
    void partFetched(QString html, qreal percentageRead, QHash<QString, QImage> url2image);
    void lastFetchedPartChanged();
    void fetchedLatestReleasedParts(const PartList& parts);
    void resetLatestReleasedParts(const PartList& parts);

    // Manga related signals
    void MangaSeriesFetched(const SeriesList& series);
    void MangaVolumesFetched(const VolumeList& volumes);
    void fetchedMangaImageLinks(QString mangaPartID, std::vector<QUrl> links);

public slots:
    // user related slots
    bool checkIfLoggedIn();
    void fetchUserData();
    void refreshVolumes();
    void login(const QString& email, const QString& pass);
    void encryptAndStorePassword(QString const & pass);

    // light novel related slots
    void fetchLNSeriesList();
    void fetchLNVolumesForID(const QString& seriesID);
    void fetchPart(const Part part, qreal percentageRead = 0.0, bool storeOffline=false);
    void updatePartCompletionStatus(const Part& part, qreal readPercentage);
    void fetchLatestReleasedParts(int offset=-1, unsigned limit=10);

#ifdef MANAG
    // manga related slots
    void fetchMangaSeriesList();
    void fetchMangaVolumesForID(const QString& seriesID);
    void fetchMangaImageURLs(QString mangaPartID);
    void getDecoderForImageURLs(QString mangaPartID, std::vector<QUrl> urls, bool save_images = true);
#endif

private:
    void loginWithStoredCredentials();
    void handleNetworkError(QNetworkReply* reply);
    QString createAccessCookiesString();
    void parseNovelString(const QString& part, qreal percentage, QString partId, bool storeOffline);
    void storeOffline(QString partId, QString html, QHash<QString, QImage> url2image);


#ifdef MANGA
    QMutex m_dedrmMutex;
    QWaitCondition m_dedrmExists;
    std::map<QString, std::unique_ptr<DeDRM>> m_mangaID2dedrm;
#endif
    observer_ptr<QNetworkAccessManager> m_manager;
    QSettings* m_settings;
    QString m_currentSeriesID;
    QString m_accessToken;
    QString m_userid;
    Part m_lastFetchedPart;
    int m_loginStatus = Pending;
    int m_alreadyFetchedCount = 0;
};

struct ImageReplyHandler : QObject
{
    Q_OBJECT

public:
    ImageReplyHandler(QString wellformedHtml, qreal percentage, int imageCount, JNovelClubConnector* connector, QString partId, bool storeOffline);

    QString m_wellformedHtml;
    QString m_partId;
    qreal m_percentage;
    QHash<QString, QImage> m_images;
    JNovelClubConnector* m_connector;
    int m_remainingImageCount;
    bool m_storeOffline;
    void tryComplete();

public slots:
    void handleReply();
};


#endif // JNOVELCLUBCONNECTOR_H
