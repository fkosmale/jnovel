#ifndef PARTLISTMODEL_H
#define PARTLISTMODEL_H

#include <QAbstractListModel>
#include "partlist.h"

class PartListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(PartListModel)

public:
    explicit PartListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    enum PartRoles {
        PartIDRole = Qt::UserRole + 1,
        TitleRole,
        TitleSlugRole,
        ExpiredRole,
        PreviewRole,
        AdapterRole,
        LaunchDateRole,
        ThumbnailRole
    };

public slots:
    void setParts(const PartList& parts);
    void addMoreParts(const PartList& newParts);

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    PartList m_parts;
};

#endif // PARTLISTMODEL_H
