﻿#include <QQuickStyle>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtQml/qqmlcontext.h>
#include <QQmlEngine>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QIcon>
#include <QQmlNetworkAccessManagerFactory>
#include <QStandardPaths>
#include <QStringBuilder>
#include <QLoggingCategory>

#include "pageview.h"
#include "commonmetadatasortfilterproxymodel.h"
#include "jnovelclubconnector.h"
#include "serieslistmodel.h"
#include "series.h"
#include "volumelistmodel.h"
#include "partlistmodel.h"
#include "fontinfo.h"
#ifdef MANGA
#include "mangaimagesmodel.h"
#include "mangaimageprovider.h"
#endif

#ifndef TESTING
#define TESTING 0
#endif
#ifdef TESTING
#include <QDirIterator>
#endif

class OnDiskCacheNAMFactory : public QQmlNetworkAccessManagerFactory
{
public:
    Q_DISABLE_COPY(OnDiskCacheNAMFactory)
    OnDiskCacheNAMFactory() : QQmlNetworkAccessManagerFactory() {}
    QNetworkAccessManager *create(QObject *parent) final;
};

QNetworkAccessManager *OnDiskCacheNAMFactory::create(QObject *parent)
{
    auto *nam = new QNetworkAccessManager(parent);
    auto diskCache = new QNetworkDiskCache();
    auto cachePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    diskCache->setCacheDirectory(cachePath);
    nam->setCache(diskCache);
    return nam;
}

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QGuiApplication::setOrganizationName("J-novel.club");
    QGuiApplication::setApplicationName("reader");
    //QLoggingCategory::setFilterRules("*.debug=false\n");
#ifdef Q_OS_WIN
    QQuickStyle::setStyle(QStringLiteral("Universal"));
#elif defined Q_OS_ANDROID
    QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);
#else
    QQuickStyle::setStyle(QStringLiteral("Fusion"));
#endif
    QQuickStyle::setStyle(QStringLiteral("Material"));
    QQmlApplicationEngine engine {};
    engine.setNetworkAccessManagerFactory(new OnDiskCacheNAMFactory);

    JNovelClubConnector connector{engine.networkAccessManager()};
#if not TESTING

    // app takes ownership of all the models
    auto LNSeriesModel = new SeriesListModel(&app);
    auto MangaSeriesModel = new SeriesListModel(&app);
    auto filterableLNSeriesModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableLNSeriesModel->setSourceModel(LNSeriesModel);
    auto filterableMangaSeriesModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableMangaSeriesModel->setSourceModel(MangaSeriesModel);
    auto LNVolumeModel = new VolumeListModel(&app);
    auto filterableLNVolumeModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableLNVolumeModel->setSourceModel(LNVolumeModel);
#ifdef MANGA
    auto MangaVolumeModel = new VolumeListModel(&app);
    auto filterableMangaVolumeModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableMangaVolumeModel->setSourceModel(MangaVolumeModel);
#endif
    auto libraryModel = new LibraryModel(&app);
    auto LNPartsModel = new PartListModel(&app);
    auto mangaPartsModel = new PartListModel(&app);
    auto latestReleasedPartsModel = new PartListModel(&app);

#ifdef MANGA
    auto mangaImagesModel = new MangaImagesModel {};
#endif

    QObject::connect(&connector, &JNovelClubConnector::LNSeriesFetched, LNSeriesModel, &SeriesListModel::setSeriesList);
    QObject::connect(&connector, &JNovelClubConnector::MangaSeriesFetched, MangaSeriesModel, &SeriesListModel::setSeriesList);

    QObject::connect(&connector, &JNovelClubConnector::LNVolumesFetched, LNVolumeModel, &VolumeListModel::changeVolume);
#ifdef MANGA
    QObject::connect(&connector, &JNovelClubConnector::MangaVolumesFetched, MangaVolumeModel, &VolumeListModel::changeVolume);
#endif

    QObject::connect(&connector, &JNovelClubConnector::loginSuccess, &connector, &JNovelClubConnector::fetchUserData);

    QObject::connect(&connector, &JNovelClubConnector::fetchedUserData, libraryModel, &LibraryModel::setUser);

    QObject::connect(&connector, &JNovelClubConnector::fetchedLatestReleasedParts, latestReleasedPartsModel, &PartListModel::addMoreParts);
    QObject::connect(&connector, &JNovelClubConnector::resetLatestReleasedParts, latestReleasedPartsModel, &PartListModel::setParts);

#ifdef MANGA
    QObject::connect(&connector, &JNovelClubConnector::fetchedMangaImageLinks, [&connector, mangaImagesModel](QString mangaPartID, auto list) {
        std::vector<QString> mangaUrls;
        mangaUrls.reserve(list.size());
        std::transform(cbegin(list), cend(list), std::back_inserter(mangaUrls), [&mangaPartID](QUrl const& url) {
            QString ret = QStringViewLiteral("image://manga/") % mangaPartID % url.toString();
            return ret;
        });        
        mangaImagesModel->setImagePath(mangaUrls);
        connector.getDecoderForImageURLs(mangaPartID, list);
    });
#endif


    QObject::connect(LNVolumeModel, &VolumeListModel::partListChanged, LNPartsModel, &PartListModel::setParts);
#ifdef MANGA
    QObject::connect(MangaVolumeModel, &VolumeListModel::partListChanged, mangaPartsModel, &PartListModel::setParts);
#endif


    connector.fetchLNSeriesList();

    qmlRegisterType<PageView>("club.jnovel.unofficial", 1, 0, "PartPageView");
    qmlRegisterType<PartListModel>("club.jnovel.unofficial", 1, 0, "PartListModel");
    qmlRegisterType<VolumeListModel>("club.jnovel.unofficial", 1, 0, "VolumeListModel");
    qmlRegisterType<CommonMetaDataSortFilterProxyModel>("club.jnovel.unofficial", 1, 0, "CommonMetaDataSortFilterProxyModel");
    qmlRegisterUncreatableMetaObject(Reading::staticMetaObject, "club.jnovel.unofficial", 1, 0, "ReadingNS", QStringLiteral("only enums!"));

    qmlRegisterSingletonType(QUrl("qrc:/qml/custom/DefaultPalette.qml"), "club.jnovel.unofficial", 1, 0, "DefaultPalette");

#ifdef MANGA
    auto mangaImageProvider = new MangaImageProvider {&connector};
    engine.addImageProvider("manga", mangaImageProvider);
#endif

    QFontDatabase::addApplicationFont(QLatin1String(":/resources/icomoon.ttf"));
    QFontDatabase::addApplicationFont(QLatin1String(":/resources/literata/literata-regular.ttf"));
    QFontDatabase::addApplicationFont(QLatin1String(":/resources/literata/literata-bold.ttf"));
    QFontDatabase::addApplicationFont(QLatin1String(":/resources/literata/literata-italic.ttf"));
    QFontDatabase::addApplicationFont(QLatin1String(":/resources/literata/literata-bold-italic.ttf"));

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
    auto root = engine.rootContext();
#define qmlRegisterSingletonInstance(a,b,c,name, value) qmlRegisterType<std::remove_pointer_t<decltype(value)>>();root->setContextProperty(name, value)
#endif    
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"FilterableLNSeriesModel",filterableLNSeriesModel);
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"FilterableMangaSeriesModel",filterableMangaSeriesModel);
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"LNVolumeModel", filterableLNVolumeModel);
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"LNPartsModel", LNPartsModel);
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"MangaPartsModel", mangaPartsModel);
#ifdef MANGA
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"MangaVolumeModel", filterableMangaVolumeModel);
#endif
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"Connector", &connector);
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"Library", libraryModel);
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"LatestReleasedParts", latestReleasedPartsModel);
#ifdef MANGA
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0 ,"MangaImagesModel", mangaImagesModel);
#endif
    FontInfo fontInfo;
    qmlRegisterSingletonInstance("club.jnovel.unofficial", 1, 0, "FontInfo", &fontInfo);

    engine.load(QUrl(QStringLiteral("qrc:qml/main.qml")));

    auto icon = QIcon(QStringLiteral(":/resources/icon.png"));
    QGuiApplication::setWindowIcon(icon);

    return QGuiApplication::exec();
#else
    // testing
    QDirIterator it("/home/fabian/testimages", QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
    while(it.hasNext()) {
        auto const id = it.next();
        auto filepath = id +  QString("/%1.png");
        auto file = QFile {filepath.arg(0)};
        file.open(QIODevice::ReadOnly);
        auto initial_image = QImage::fromData(file.readAll());
        Images images {};
        for (int i = 1;i<6;++i) {
            QFile file {filepath.arg(i)};
            file.open(QIODevice::ReadOnly);
            images.push_back( QImage::fromData(file.readAll()) );
        }
        DeDRM dedrm {images, initial_image};
        if (!dedrm.canDecrypt()) {
            qWarning() << "Could not decrypt" << id<< "!";
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
    // end testing
#endif
}
