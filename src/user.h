#ifndef USER_H
#define USER_H

#include <QString>
#include <QHash>

#include "volumelist.h"

class QJsonObject;

struct CompletionStatus {
    qreal maxCompletion;
    qreal completion;
};

class User
{
public:
    User();
    static User fromJSON(const QJsonObject &json);
    QString getId() const;

    QString getName() const;

    QString getEmail() const;

    VolumeList getLibrary() const;

    QHash<QString, CompletionStatus> getPart2completionStatus() const;

    void setPartCompletion(const QString& partID, qreal percentRead);

private:
    QString id;
    QString name;
    QString email;
    VolumeList library;
    QHash<QString, CompletionStatus> part2completionStatus;
};

Q_DECLARE_TYPEINFO(User, Q_MOVABLE_TYPE);

#endif // USER_H
