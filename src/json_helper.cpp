#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValueRef>
#include <QJsonValue>

#include "json_helper.h"

ParsedAttachment parseAttachments(const QJsonArray& attachments)
{
    ParsedAttachment parsed;
    for (const auto& attachment: attachments) {
        if (attachment.isObject()) {
            const auto attAsObject = attachment.toObject();
            const auto fullPath = attAsObject[QStringLiteral("fullpath")].toString();
            if (fullPath.contains(QLatin1String("thumbnail"))) {
                parsed.thumbnailPath = fullPath;
            } else if (fullPath.contains(QLatin1String("cover"))) {
                parsed.coverPath = fullPath;
            }
        }
    }
    return parsed;
}

QString partJson2thumbnailPath(const QJsonObject& part)
{
    const auto attachments = part[QStringLiteral("attachments")].toArray();

    for (const auto& attachment: attachments) {
        if (attachment.isObject()) {
            const auto attAsObject = attachment.toObject();
            const auto fullPath = attAsObject[QStringLiteral("fullpath")].toString();
            if (fullPath.contains(QLatin1String("thumbnail"))) {
                return fullPath;
            }
        }
    }
    return {};
}

