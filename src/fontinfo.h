#ifndef FONTINFO_H
#define FONTINFO_H

#include <QObject>
#include <QList>
#include <QFontDatabase>
class QFont;

class FontInfo : public QObject
{
    Q_OBJECT
public:
    explicit FontInfo(QObject *parent = nullptr);
    Q_INVOKABLE QList<int> getFontSizes(const QFont &font);

private:
    QFontDatabase m_fdb;
};

#endif // FONTINFO_H
